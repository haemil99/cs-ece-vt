\subsection{Task 3: Achieving Lightweight and Flexible Soft Error Resilience}
\label{sec:reliability}
\begin{wrapfigure}{L}{0.4\textwidth}
\centering
\includegraphics[width=0.4\textwidth]{figs/spec.pdf} 
\scriptsize
\caption{Peformance-Reliability Tradeoff}
\label{fig:spec}
%\vspace{-0.3cm}
\end{wrapfigure}
Due to technology scaling, circuits are getting more sensitive to
radiation-induced soft errors that are mostly caused by cosmic rays as well as
alpha particles from packaging
material~\cite{jang11,mukherjee05,UpasaniVG12,UpasaniVG13,UpasaniVG14,UpasaniVG15,DeBardeleben14}.
Soft errors may lead to application crash or even worse, silent data corruptions
(SDC) that are not caught by the error detection logic but may cause the program
to produce incorrect output.  In the context of UAVs, soft error resilience is
indeed indispensable because their fault can lead to physical demange or even
loss of
life~\cite{vivekanandan2016simplex,coopmans2014cyber,hoffmann2014practitioner,vega2015resilient}.

Unfortunately, it is impossible to protect commodity UAVs from soft errors by
using existing solutions.  The reason is that they are either based on special
hardare support (AR-SMT~\cite{rotenber99}, RMT/CRT~\cite{crt2002},
ARGUS~\cite{meixner2007argus}, Razor~\cite{ernst2004razor,ernst2003razor},
Adaptive-Pipeline~\cite{natarajan2016self,natarajan2014timing}) or
resource-consuming due to software implementation (nZDC~\cite{nzdc16},
CEDA/CFCSS~\cite{ceda11,OhSM02a}, TRUMP~\cite{chang07},
Containment-Domains~\cite{chung2012cds}, VOCL-FT~\cite{pena15}). Even, the most
recent software solutions such as InCheck~\cite{didehban2017integrated} and
NEMESIS~\cite{didehban2017nemesis} still slowdown applications more than 2.9x on
average.

More importantly, all the prior works do not provide any technique for
parameterized performance-reliablity tradeoff.  
Figure~\ref{fig:spec} shows the high-level idea of such a tradeoff.
Therefore, they cannot adapt to
the varying requirment for performance and realbility on enviromental changes of
deployed UAVs.

Addressing the above problems poses a daunting challenge, i.e., how to provide
commodity UAVs with a lightweight and flexible resilience solution.  Currently,
we are not aware of any existing technique which can achieve that.  Thus,
{\it there is an urgent demand of innovation to achieve practical UAV resilience
that can efficiently deal with soft errors in a lightweight and, more
importantly, flexible manner.  }

\subsubsection{Task 3.1: Compiler-Directed Lightweight Soft Error Resilience} \label{sec:bolt-dmr}
To protect commodity UAVs from soft errors without special hardware support, we
propose compiler-directed, lightweight, flexible soft error resilience.  It is
essential to have both the detection and the correction of soft errors for
complete resilience. 

For soft error detection, this task will leverage
SWIFT, i.e., an \textit{instruction duplication}~\cite{reis05} scheme; it replicates each
instruction and its registers, compares the values of the original and the
replica at a given synchronization point, e.g., before
store instructions, and raises an alarm when the values differ from each other.
According to the original paper of SWIFT~\cite{reis05}, it incurs
$\approx$50\% exection time overhead. 

To recover from soft errors detected by the instruction duplication, this task
will use PI Jung's idempotence recovery work called Bolt~\cite{liuSC16}. 
An idempotent region is a part of program code that can be freely re-executed to
generate the same output.  Thus, soft error recovery can be achieved by simply
jumping back to the beginning of the idempotent region that encounters an error.

To this end, Bolt compiler partitions a program into a series of  idempotent
regions~\cite{idem12,dekruijf12}, and 
checkpoints the output of each region---the registers that are modified by the region and 
serve as input to some following region. In data flow terms, 
the outputs of region $r$ is the live-out registers defined in the region---the values that
are written and downward-exposed~\cite{muchnick97}: $$ckpt_{r} = Def_{r}
\bigcap LiveOut_{r}$$
where $Def_{r}$ is the set of registers defined in $r$ and $LiveOut_{r}$ is the
set live-out registers of $r$. 

In summary, the recovery protocol will be as follows:
once a soft error is detected by SWIFT, Bolt’s runtime system takes program control to
execute a recovery block that restores all the input registers of the
faulty region; after that, the program data remain the same as they were at the be-
ginning of the region before the error. Finally, Bolt redirects the
program control to the entry of the faulty region and restarts from it thereby
correcting the error with the help of the idempotence.

\paragraph{Checkpoint Scheduling}

\begin{wrapfigure}{L}{0.52\textwidth}
\centering
\includegraphics[width=0.52\textwidth]{figs/checkpoint_scheme.pdf} 
\scriptsize
\caption{The impact of checkpoint scheduling}
\label{fig:aaa}
\vspace{-0.3cm}
\end{wrapfigure}

Currently, Bolt's idempotence-based recovery incurs 4.7\% execution time
overhead. Thus, when combined with soft error detection, i.e., Bolt+SWIFT (, the
runtime overhead would be significant ($\approx$54.7\%).  With that in mind, we seek to reduce the
number of checkpoints, i.e., store instructions to preserve output registers of
each region.  The key observation is that there is a freedom to place checkpoint
in an appropriate idempotent region; of course, we need to carefully place a
checkpoint taking into account the resulting performance overhead. In a sense,
this is essentially a checkpoint scheduling problem. To derive a optimal
solution, we will evaluate the cost of each candidate point for checkpoint
insertion.


Figure~\ref{fig:aaa} (a) and (b) show a checkpoint scheduling example from {\it libquantum}
in SPEC2006 benchmark suite. 
This program is divided into several idempotent regions indicated by the dashed lines.
Only the last region of basic block 3,
 has three live-in registers with anti-dependence, i.e., RBX, RBP and R13. 
In Figure~\ref{fig:aaa} (a), checkpoints are inserted in a lazy manner, i.e.,
they show up at the entry of a region. 
Figure~\ref{fig:aaa} (b) demonstrates the advantage of eager checkpointing. Both
checkpoints for RBP and RBX are hoisted to BB1. R13 is checkpointed two times at BB2 and
BB3 in the eager manner that immediately checkpoints the last update.
Here, huge performance benefit can be achieved as the checkpoints for RBP and
RBX are no longer inside the loop. 
Note that we have also found many other examples in which lazy checkpointing beats the eager one for the similar reason, i.e., the reduced number of checkpoints.
Figure~\ref{fig:aaa} (c) indicates that the hybrid of the lazy and eager schemes referred to as
"optimal checkpointing" can further reduce the number of checkpoints being inserted.
Here, one checkpoint for R13 in BB2 is removed in this optimal manner.

To realize this idea, we propose static and dynamic checkpoint cost evaluation.
we will first perform postpass optimization,
that analyzes definition-use relation for those registers with anti-dependence,
 to estimate the static cost of each checkpoint candidate; this will be implemented using traditional compiler techniques such as {\it use-def} chain construction~\cite{muchnick97}. 
Then, dynamic profiling information will be leveraged to increase the precision of the
statically estimated cost.

\subsubsection{Task 3.2: Parameterized Performance-Reliability Tradeoff} \label{sec:compiler-opt}
UAVs can be leveraged for multiple purposes and be deployed in a variety of
of environments.  The implication is that once UAVs are deployed, their
operating conditions can vary over time according to environmental change.
Thus, the level of required performance/realiability can dynamically vary as
well.

In light of this, the goal of this task is to develop a novel soft error
resilience mechanism that enables a parameterized performance-reliability
tradeoff, i.e., the ability to achieve an appropriate level of reliability
proportional to the paid performance overhead.  The takeaway is that the proposed soft error
resilience allows UAVs (and their administrators) to dynamically adjust the
reliability of the tasks to be executed taking into account other constraints,
e.g., schedulability in Section~\ref{sec:scheduling}.

To the best of our knowledge, our proposal will be the first work for achieving
the parameterized performance-reliability tradeoff.  Our key \textbf{novelty} is
the ability to equip each task code with a reliability knob that can be dynamically
controlled after the deployment.

\paragraph{Achieving Proportinal Reliability}
\begin{wrapfigure}{L}{0.5\textwidth}
\centering
\includegraphics[width=0.5\textwidth]{figs/xform.pdf} 
\scriptsize
\caption{Multi-versioning and code transformation for parameterized
				performance-reliablity tradeoff}
\label{fig:xform}
\vspace{-0.3cm}
\end{wrapfigure}


To realize the idea of paramiterized performance-reliability tradeoff , this
task proposes compiler multi-versioning and its code transformation that can
control the number of instructions to protect aginst soft errors.  
In the first
place, our compiler will prepare three versions of each task code.
Figure~\ref{fig:xform} (a) and (b) show the original compute-intensive loop of each
task and its soft error resilient code, respectively, using the control-flow
graph~\cite{muchnick97} of the task code.
As shown in Figure~\ref{fig:xform}(b), the
original loop body is lengthened to represent the overhead of hardening it with SWIFT+Bolt. 

The main idea of our code transformation is to restructure the program control-flow 
to alternate between the original loop body and the hardened loop body.
Figure~\ref{fig:xform} (C) shows how we achieve the parameterized
performance-reliability based on the code transformation. In the figure, the
black box of the control flow graph represents the conditional branch to decide
which loop body needs to be executed, i.e., this acts like a slider bar ($\blacktriangledown$) in the reliability spectrum shown in Figure~\ref{fig:spec}.
The pseudo code is like below:
{\footnotesize
$$JumpTarget = (rand()\%100 <
ReliabilityDemandPercent)\ ?\ HardenedLoopBody : OriginalLoopBody;$$
}
Especially, the transformed code takes the reliability demand as a percentage from the computation/reliailibty map
learned in Section~\ref{sec:mapping}, as a parameter that can be determined to the
code at runtime.
In this way, we believe that it would be possible to achieve an appropriate level of reliability proportional to
the paid performance overhead; this will be validated by statistical fault
injection campaign measuring the resulting reliability for different reliability
demand parameters in the above pseudo code. 

Once the task version is selected according to the reliability demand,
we need to inform the task scheduler (Section~\ref{sec:scheduling}) of the execution time.  
With that in mind, we will develop regression models to estimate the execution
time of the selected task version based on the 
time of the previously selection task version.
That is, after the version selection, we will start measuring the execution time
of the selected task to be scheduled. 
Finally, we plan to develop another regression model that can estimate the
power consumption of the task being launched based on its estimated execution time.



