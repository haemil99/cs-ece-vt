\subsection{Task 2:  Runtime Resource Optimization} \label{sec:scheduling}

UAVs are highly safety-critical while being sensitive to various costs such as size, weight, and power (SWaP). The former requires resource provisioning that accounts for the worst case, even if such a case occurs infrequently, while this is in conflict with the latter requirement. In order to manage both of these aspects at the same time, we propose to assign resources at runtime only when needed, according to the physical dynamics of the system and the operating environment. 

Under this setting, now all the methods become in-field, including the analysis that makes sure safety-critical requirements are all met, and the optimization of resource allocations that respects the critical constraints while optimizing certain objective. However, this imposes grand challenges, as such methods are already difficult to scale even for design-time usage (which may take days or weeks). Hence, we need fundamentally different, paradigm-shifting new approaches. 

\paragraph{Novelty claims: Why our approach is paradigm-shifting}
Many have contributed to time-critical CPS. In particular, \emph{safe} timing analysis techniques are developed for validating the system schedulability. However, the optimization of time-critical CPS is greatly overlooked despite its vast potential. The current mindset is (1) to apply existing safe analysis techniques developed for design validation, and (2) to directly use standard optimization frameworks such as Integer Linear Programming (ILP) and their solvers. \textbf{Our proposed techniques challenge this mindset. Each of them potentially provides orders of magnitude in algorithm efficiency improvement without loss of solution quality.}

\begin{itemize}
\item \textbf{Research Task 2.1: developing novel timing analysis techniques for optimization}. \\
\textbf{Rationale:} \emph{Unsafe} analyses cannot be used in validation, as they may give false positives on timing correctness. But they are a powerful technique in optimization: a close-to-exact unsafe analysis can quickly detect solution infeasibility and provide tight bound on optimization objectives for pruning. Hence, we propose to develop new unsafe timing analysis techniques.

\item \textbf{Research Task 2.2: developing a new domain-specific optimization framework for time-critical CPS}. \\
\textbf{Rationale:} Leveraging standard optimization frameworks is appealing, as modern solvers such as CPLEX for ILP are highly polished. However, it simply abandons some beautiful results from the domain experts. For example, Audsley's Algorithm~\cite{AudsleyReport1991} can find a schedulable priority assignment for a large class of systems if there exists one, which only checks a quadratic number of priority orders out of the exponentially many. Given an infeasible priority assignment, our optimization algorithm uses a modified Audsley's Algorithm to efficiently generalize to many other infeasible ones. By doing so, it also avoids the complication of formulating the schedulability conditions. Hence, \textbf{we propose to leverage domain knowledge} and develop transformative optimization algorithms.
\end{itemize}

Yet for time-critical CPS, our techniques are \textbf{comparable or even more general} in applicability than some of the standard optimization frameworks. For example, timing analysis for many systems is impractical to be formulated in ILP~\cite{ZengTC2013,ZhaoEMSOFT2017}, but can be readily handled by the proposed framework in Task 2.2: our framework uses a generic  procedure for timing analysis (instead of formulating it in a particular form).


\paragraph{Relationship to co-PI's Expertise.}
The proposed research is built upon co-PI Zeng's substantial experience and recent work. In 2017-2018, a set of promising preliminary results have been accepted in highly-ranked venues (DATE, RTSJ, EMSOFT, ICCAD, RTSS, RTAS)~\cite{ZhaoDATE2017,ZhaoRTSJ2017,ZhaoEMSOFT2017,RaoICCAD2017,ZhaoRTSS2017,ZhaoRTAS2018}. Dr. Zeng has made many other contributions to the optimization of time-critical CPS~\cite{ZengDAC2009, GhosalDATE2010, ZengTII2011, ZengSIES2012, ZhuTECS2012, ZengETFA2012, MehiaouiLCTES2013, HanDATE2014, DongDATE2014, ZengTECS2014, WozniakASE2014, AlbayatiRTAS2015, WangECRTS2015, WangTPDS2016, WangTODAES2016, GuTPDS2016, ZhaoJSA2017, JoshiECRTS2017, JoshiRTSS2017}. He received four {\em best paper/best student paper awards}, including one from RTSS 2017 and another from ECRTS 2013 (both are top conferences on real-time systems)~\cite{ZengECRTS2013,ZhaoRTSS2017} and five other best paper nominations in related fields.

\subsubsection{Task 2.1: Developing new timing analysis for optimization}
\label{sec:task1}


% \noindent
% \textbf{Related work.}
% A major research problem in time-critical CPS is timing analysis, to predict whether tasks can meet their deadlines. The past research has delivered results that are suitable for design validation under various settings. However, \emph{analysis techniques for use in optimization are almost never studied}.

% \begin{figure}[htp]
%   \vspace{-2ex}
%   \centering
%   \begin{overpic}[width=0.8\linewidth]{figs/Schedulability.pdf}
%   \scriptsize
%     \put(95.7,14.5){\cite{LiuJACM1973}}
%     \put(95.9,20){\cite{BiniTC2003}}
%     \put(95.7,24.5){\cite{SjodinRTSS1998}}
%     \put(95.7,29){\cite{BiniTC2015}}
%     \put(95.7,33){\cite{BiniTC2009}}
%     \put(95.7,38){\cite{BiniRTSS2015}}
%     \put(95.7,64){\cite{BiniRTSJ2008}}
%     \put(42,30){\cite{AudsleySEJ1993}}
%     \put(44.1,34.3){\cite{LehoczkyRTSS1989}}
%     \put(50.4,38){\cite{BiniTC2004}}
%     \put(67.5,41){\cite{ZengTC2013} [This proposal]}
%   \end{overpic}
%   \vspace{-2ex}
%   \caption{The evolution of timing analysis for Liu-Layland (LL) task model scheduled with static priority.}
%   \label{fig:Schedulability}
%   \vspace{-1ex}
% \end{figure}

% For example, even the elementary case of {\em Liu-Layland (LL)} task model (independent periodic tasks with deadline equal to period) scheduled with static priority on single-core~\cite{LiuJACM1973} shall be revisited. There are three categories of techniques (Fig.~\ref{fig:Schedulability}). The {first} category is \emph{worst-case response time (WCRT) based exact analysis}~\cite{AudsleySEJ1993}. However, WCRT-based analysis requires an iterative procedure to calculate WCRT. For direct use in optimization frameworks such as ILP, it requires many integer variables (quadratic in the number of tasks) and is unlikely to be scalable~\cite{ZengTC2013}. The {second} is {\em sufficient-only analysis}, which provides an {under-approximation} of the schedulability region~\cite{BiniTC2003,BiniTC2015,SjodinRTSS1998,BiniTC2009,BiniRTSS2015}. However, global optimum is not guaranteed in optimization since the search is in a smaller feasibility region. The {third} is \emph{necessary-only analysis}, which gives an {over-approximation} of the schedulability region. This category is completely overlooked, except the trivial EDF bound~\cite{BiniRTSJ2008} that the total task utilization is no more than 100\%.

\noindent
\textbf{Summary of our approach.}
We use an example to illustrate. Consider two LL tasks $\tau_1$ and $\tau_2$. Their periods are $T_1 = 4$ and $T_2 = 10$ respectively. $\tau_1$ has a higher priority than $\tau_2$. Fig.~\ref{fig:GeometricCombination} plots the exact schedulability region (in the shadowed area), which is the union of two triangular areas (i.e., two hyperplanes).

Our proposal is to develop fast necessary-only analysis that is close to be exact. This type of analysis over-approximates the schedulability region. For example, in Fig.~\ref{fig:GeometricCombination} the striped area denotes the over-approximation of the proposed geometric bound.


\begin{figure}[htp]
  \centering
  \begin{overpic}[width=0.8\linewidth]{figs/GeometricCombination.pdf}
  \end{overpic}
  \vspace{-4ex}
  \caption{An example to illustrate the feasibility region in task WCETs $\{C_1,C_2\}$ for two LL tasks, with periods $T_1 = 4$ and $T_2 = 10$ respectively. The shadowed area is the exact schedulability region $\mathbb{M}$. The striped area denotes the over-approximation from the geometric bound $\mathbb{M}^{GB}(-\frac{1}{2})$. The EDF bound $\mathbb{M}^{EDF}$ provides the outmost triangle.}\label{fig:GeometricCombination}
\end{figure}



An unsafe (hence necessary-only) analysis over-approximates the schedulability region. This type of analyses is overlooked due to the validation-oriented mindset. Historically the research on timing analysis focused on validation: for time-critical CPS, the {\em final design} shall always satisfy system timing correctness, but necessary-only analysis techniques may give false positive on system feasibility.


However, {\em necessary-only analyses are very useful in optimization}. Unlike the under-approximation provided by the sufficient-only analysis, searching in the over-approximation of the feasibility region will never miss the true optimum. With carefully designed optimization procedures, such as a sanity check and refinement after returning an infeasible solution, it gives the promise of providing better designs than a sufficient-only analysis. In fact, most modern optimization solvers leverage over-approximations (relaxations) of the feasibility region. For example, ILP problems are generally solved with a linear programming based branch-and-bound algorithm~\cite{ILP}. The algorithm extensively leverages the linear programming relaxation where all integrality constraints are removed.


\paragraph{Technical challenges.}
There are two difficulties for a necessary-only analysis to be useful. The analysis shall allow the feasibility region to be much easier to search during the optimization step. The over-approximation shall be as small as possible such that the likelihood of infeasible solution is minimized.


A well-known trivial condition is that the total utilization on a CPU cannot exceed 100\% (denoted as {\em EDF bound} below). Fig.~\ref{fig:GeometricCombination} illustrates the EDF bound $\mathbb{M}^{EDF}$ for the schedulability region $\mathbb{M}$ in the example. As shown in the figure, the EDF bound is already the tightest {\em convex} condition we can find: anything tighter than that is non-convex. But it still has a fairly large over-approximation.

Specifically, the exact schedulability region $\mathbb{M}$ can be expressed as (where $\mathbf{C} = \{C_1, C_2\}$ is the vector of WCETs):
%
\[
\begin{array}{c}
\mathbb{M} = \mathbb{F}_A \bigcup \mathbb{F}_B,
\text{ where } \mathbb{F}_A = \{\mathbf{C} > 0 | \frac{C_1}{4} + \frac{C_2}{8} \le 1\},
\mathbb{F}_B = \{\mathbf{C} > 0 | \frac{3 C_1}{10} + \frac{C_2}{10} \le 1\}
\end{array}
\]
%
If we consider two points  $\mathbf{C}_A \in \mathbb{F}_A$ and $\mathbf{C}_B \in \mathbb{F}_B$, the affine combination is a linear combination $\mathbf{C} = \alpha \mathbf{C}_A + \beta \mathbf{C}_B$, where the coefficients $\alpha$ and $\beta$ are positive and satisfy $\alpha+\beta \le 1$. We can see from Fig.~\ref{fig:GeometricCombination} that $\mathbb{M}^{EDF} = \{\mathbf{C} > 0 | \frac{C_1}{4} + \frac{C_2}{10} \le 1 \}$ is exactly the affine combination of $\mathbb{F}_A$ and $\mathbb{F}_B$:
%
\[
\mathbb{M}^{EDF} =  \{ \mathbf{C} | \mathbf{C} = \alpha \mathbf{C}_A + \beta \mathbf{C}_B,  \mathbf{C}_A \in \mathbb{F}_A,  \mathbf{C}_B \in \mathbb{F}_B, \alpha+\beta \le 1, \alpha \ge 0, \beta \ge 0\}
\]

\paragraph{Preliminary study.}
Our key observation is that the analysis condition {\em does not have to be convex} to be useful. There are optimization frameworks that can handle non-convex constraints efficiently. For example, Fig.~\ref{fig:GeometricCombination} provides such a condition $\mathbb{M}^{GB}(-\frac{1}{2})$ that can be formulated in {\em geometric programming}, which can be solved very efficiently (i.e., with polynomial time complexity)~\cite{BoydGeo2007}. Its over-approximation is only 3\% of that from the EDF bound.

Specifically, we coin the term \emph{geometric bound (GB)} to illustrate our idea to formulate a series of close over-approximations $\mathbb{M}^{GB}(\gamma)$ with $\gamma < 0$. The component-wise power of each point in $\mathbb{M}^{GB}(\gamma)$ is lower bounded by the affine combination of those in $\mathbb{F}_A$ and $\mathbb{F}_B$:
%
\[
\mathbb{M}^{GB}(\gamma) =  \{ \mathbf{C} | \mathbf{C}^{\gamma} \ge \alpha \mathbf{C}_A^{\gamma} + \beta \mathbf{C}_B^{\gamma},  \mathbf{C}_A \in \mathbb{F}_A,  \mathbf{C}_B \in \mathbb{F}_B, \alpha+\beta \le 1, \alpha \ge 0, \beta \ge 0 \}
\]

\noindent
Any $\gamma < 0$ is guaranteed to provide a better approximation than $\mathbb{M}^{EDF}$. Fig.~\ref{fig:GeometricCombination} shows an example $\mathbb{M}^{GB}(-\frac{1}{2})$ where $\gamma = -\frac{1}{2}$. Finally, it is easy to see that $\mathbb{M}^{GB}(\gamma)$ can be formulated in geometric programming without introducing any discrete (binary or integer) variables.

With this new analysis, a set of optimization problems can be handled. For example, systems supporting DVFS (e.g., battery-powered drones) tune voltage/frequency to save power and energy. The power consumption is described with a polynomial function of CPU frequency, and the execution time is a linear function of frequency~\cite{BambaginiTECS2016}. All of these are readily handled by the geometric programming framework.

\subsubsection{Task 2.2: Devising Domain-Specific Optimization Algorithms}\label{sec:optalgorithms}

The new timing analysis techniques in Section~\ref{sec:task1} already provide {\em rich opportunities} for developing new optimization algorithms. For example, a necessary-only analysis can be used to quickly prune non-optimal solutions in branch-and-bound algorithms: in a branch if the over-approximation of the feasibility region from the necessary-only analysis still only allows a solution worse than the current optimal, then this branch can be safely cut. It also provides an worst-case estimate on the suboptimality for the current best solution.

Below we detail other initial ideas on the optimization framework for time-critical CPS.

% \paragraph{Related work.}\label{sec:relatedopt}

% For the optimization of time-critical CPS, one current approach is to develop heuristics that are strongly problem-specific and cannot be generalized to other similar problems~\cite{HanDATE2015, KashifRTAS2016, AlhammadRTAS2016, BiondiICCPS2016}. The second approach is based on metaheuristics such as simulated annealing~\cite{KimICCAD2014} and genetic algorithm~\cite{KangDAC2014}. The third approach is to directly formulate the problem in standard frameworks such as branch-and-bound (BnB)~\cite{AltmeyerECRTS2014} and integer linear programming (ILP)~\cite{AzimDATE2014, AkessonRTAS2015}. These optimization algorithms are either suboptimal with no guarantee on solution quality, or only scalable to small case studies (compared to, e.g., the automotive engine control system containing over {\em a thousand} functions~\cite{PanicCODES2014, KramerWATERS2015}).

\paragraph{Our approach.}
Leveraging standard optimization frameworks (such as ILP and BnB) is a prevailing approach. But this mindset has two issues. First, not all timing analysis techniques can be formulated in a particular framework such as ILP~\cite{ZengTC2013}. Even if it is possible, the effort is considerable. Second, it ignores the power of some domain-specific algorithms, and the generic constraint solvers cannot match that. This is overlooked as such algorithms only solve a subproblem of the overall problem.


%
\begin{wrapfigure}{R}{0.5\textwidth}
\centering
\includegraphics[width=1.0\linewidth]{figs/UnschedCore.pdf}
\vspace{-3ex}
\caption{The two-step iterative optimization framework.}
\label{fig:TwoStep}
\end{wrapfigure}
%
For example, for many systems Audsley's algorithm~\cite{AudsleyReport1991} can efficiently find a feasible priority assignment if there exists one~\cite{DavisJSA2016}: it only needs to check $O(n^2)$ priority assignments out of the  $n!$ possible ones for systems with $n$ tasks. This observation leads us to propose the concept of \emph{unschedulability core}, which is essentially a minimal subset of priority partial orders that can not be simultaneously satisfied by any feasible priority assignment. For example, in a system with three tasks $\tau_1,\tau_2,\tau_3$, an unschedulability core may specify that if task $\tau_1$ has a lower priority than $\tau_2$, then the system is infeasible regardless of the other priority orders.

Our optimization framework (Fig.~\ref{fig:TwoStep}) judiciously combines the strength of two techniques: the commercial ILP solver CPLEX (or other appropriate generic constraint solvers) that is tuned for generic branch-and-bound based search, and the efficient Audsley's algorithm for checking system schedulability. The framework contains two iterative steps, as illustrated in Fig.~\ref{fig:TwoStep}. The first step ({\em Step 1}) is an ILP formulation $\Pi$ that avoids the {\em direct formulation} of the real-time schedulability region. Gradually, it adds constraints on the variables of priority orders $\mathbf{P}$  (and implicitly the schedulability constraints) depending on the results of the second step. Given a valuation on the priority order variables  $\mathbf{P}$ from Step 1, the second step ({\em Step 2}) either confirms that the tasks are schedulable, or uses a modified Audsley's algorithm to learn a summation, in the format of an unschedulability core, on why they are not.

The concept of unschedulability core is instrumental in developing the optimization framework. Each time in Step 1 we find that the system is infeasible because some tasks violate their deadlines, instead of directly adding the schedulability constraints of these tasks, we seek to add constraints on $\mathbf{P}$ based on the unschedulability core as a much more compact representation. It also tries to generalize to rule out other similar unschedulable priority orders: the unschedulability core is essentially the fundamental reason why the unschedulable priority assignment is infeasible.

\emph{The intelligence of our algorithm cannot be matched by generic constraint solvers like CPLEX}. For example, the cutting plane technique in CPLEX only rules out {\em fractional} solutions. Our technique is to avoid adding those sophisticated timing constraints to ILP in the first place, and let the modified Audsley's algorithm to quickly prune many {\em infeasible integer} solutions.


So far we focus on the case that Audsley's algorithm is applicable. The framework can be extended to those task models and scheduling policies where Audsley's algorithm is not directly applicable. The idea is that instead of the set of priority orders $\mathbf{P}$, the interfacing variables between Step~1 and Step~2 shall contain enough information that allows Audsley's algorithm to be applicable. For example, event-triggered activation makes the jitter (and consequently the response time) of a task dependent on those of the task writing to it. But if the writing task's response time is known, then Audsley's algorithm is applicable. Hence, the interfacing variables shall contain a lower bound on task response times.


\paragraph{Preliminary results.}
As a preliminary study, we apply the proposed framework to optimize the implementation of mixed-criticality automotive control models, with encouraging initial results. For systems with 35 functional blocks, the unschedulability core guided techniques are more than 1,000$\times$ faster, and the improvement is larger with larger systems. 