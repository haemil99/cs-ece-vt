\subsection{Task 4: Physical-State Checkpointing}
Embedded computing devices extensively rely on sensed information from the physical world (including the physical device being controlled -- the plant -- and the physical environment surrounding the system) for their decision making. For example, the artificial pancreas keeps the blood glucose concentration in equilibrium based on measured glucose concentrations (and additionally the sport activity) of the patient. Hence, exclusively performing checkpointing on the program state is insufficient, as the sensor nodes may lack adequate energy to make enough progress.



\subsubsection{Task 4.1 Physical State Recovery}

\noindent{\bf Novelty Claim: }
The past research has focused on checkpointing the program states for stand alone computer systems, such as~\cite{PlankATC1995}. However, checkpointing for systems that tightly integrate with the physical world is significantly different. First, it is very costly, and often simply impossible to roll back to the previous states of the physical world. Even if the controlled plant can go back to the previous state, the surrounding environment often cannot.  In this sense, embedded systems shall use roll-forward recovery to deal with the irreversible physical world. Second, the definition of a consistent state in the physical world shall have a notion of physical time. However, in computer systems, the consistent state is based on the reason of logic order~\cite{ElnozahyCUSR2002}. This makes the consistent states for physical world substantially differ from that of the computer systems (program state). We propose to develop a novel, generally applicable procedure for physical-state checkpointing in this task.

\noindent{\bf Consistent Physical States: }
The simplest, most intuitive definition of a checkpoint for the physical states is a snapshot, i.e., the values of all the physical quantities at the same point in time. In this way, we can perform checkpointing to preserve a self-consistent past image of the physical world. It is also aligned with the foundation of modern control theory, that the state-space equations are written in a way that all (input, output, and state) variables take value at the same current time.

However, this definition is based on an outstanding assumption that is often unachievable in distributed computing systems: the distributed sensing nodes should share a common notion of time and they are operating at the exact same time instant (or the error is small enough).
Obviously, this is impractical especially for nodes powered with harvested energy, as perfect or near perfect clock synchronization is complex, energy consuming, and requiring an external accurate clock (such as atomic clocks or GPS).

We propose to design a specialized protocol for checkpointing physical states. It shall find the right balance among the energy consumption and precision of the clock synchronization protocol, and the quality of the control. Specifically, a global synchronization protocol where there is a constant global timescale throughout the network will require an extensive amount of energy, but can provide high precision for establishing a global time base, and consequently ease the design of control algorithms. On the other hand, a simpler protocol can establish relative timing in which the network clocks are independent of each other and the nodes keep track of drift and offset, but it imposes a stronger requirement on the control design and may make the system unstable.

\noindent{\bf Recovery by Rolling Forward: }
Starting from a consistent physical state, we propose its recovery is to roll forward to the current time, to deal with the possible unavailability of sensor data. We will differentiate two sets of sensor data, the first set has new data received from the last control cycle, while the second has not (possibly due to lack of energy at the sensor node, or transmission error). For the first set, we predict the related physical states based on the updated sensor measurement. For the second, the prediction is based on historical information from the failed sensors themselves along with the current data measured for the first set.

We note that the proposal of rolling forward also gives another advantage. In computer program checkpointing, it is possible that to reach a global consistent state, nodes may be dragged by other nodes and forced to roll back to their earlier checkpoints (not the newest one), in the extreme case even to the system initial state~\cite{ElnozahyCUSR2002}. This possible domino effect is naturally avoided by the roll-forward mechanism: we always use known past state to estimate the future state in the physical world, and the set of failed states are handled separately from the stored states.

\subsubsection{Task 4.2 Coordinated System-wide Checkpointing }
\noindent{\bf Novelty Claim: } Inherited from the fact that we are the first to propose the concept of physical state checkpointing, the coordinated system-wide checkpointing (including both program state and physical state) will be novel as well.

\noindent{\bf Coordinated Program State and Physical State Checkpointing: }
As a whole, having separately consistent checkpoint for the program state and physical state does not necessarily mean the composed system state is consistent. For example, the control program, which consists of three steps of sensing, control effort computation, and actuation, may be interrupted after all the sensor data are collected. It has to sleep for an extensive amount of time to wait for enough harvested energy. Let us start with the simple control system structure that the sensing, control, and actuation all are executed on the same computer node, hence these operations are based on a perfect common clock source. The checkpoint of the physical state at its sleep time reflects the sensed physical information. However, after it is woken up, if we simply recover from this checkpoint and resume to compute the control effort, it is based on the physical state that was stored a while back and is now possibly obsolete.

We propose the concept of \textit{coordinated program state and physical state
checkpointing}. Specifically, there are two possible solutions. The first is
that the control program should be atomic (Task 1/2 can be extended to form a 
atomic region and ensure its rollback-free execution) in finishing the three steps of sensing, control, and actuation. The second is that the sensing step can be skipped, but the control effort should be compensated based on a prediction of the physical states that takes into consideration the time elapsed since the last checkpoint. Nevertheless, the region formation shall consider the requirement of coordinated system-wide checkpointing.

\ignore{
\subsubsection{Task 4.3 Checkpointing-Aware Control Algorithm Design}
\noindent{\bf Novelty Claim: } The design of control with roll-forward physical state recovery will bring unique challenges than existing control systems, hence will advance the state-of-the-art approaches. Simply speaking, it relies on a partial, possibly sampled at different times, understanding of the physical states, hence with fundamentally different assumptions than the feedback-based closed loop control design.

\noindent{\bf State Estimation Error: }
Assume that the known physical state for a state variable $x_i$, while computing the control effort at time $t$, is the sampled value at time $t_i < t$. We shall estimate the state of $x_i$ at time $t$, namely $x_i(t)$, based on $x_j (t_j), \forall j$. Here the estimation error may come from two sources. One is that the error estimation, as a prediction of future based on history, naturally can be inaccurate. The other is that the timestamps of the state variables may not be precise as these states may be sent from remote sensor nodes that are not perfectly synchronized.

\noindent{\bf Speculative Waiting: }
It is intuitive that the estimation error grows with the time difference between the computation of the control effort and the sensing time. Hence, if there is a possible way to speculate that in the near future there are new, valuable sensor data coming, it can be beneficial to wait for the new data. This can help bound the estimation error and help stabilize the control system. We term this as \textit{speculative waiting}. Of course, if the waited time will be too long or the new data are not expected to be substantially different from the historical data, then speculative waiting is not as valuable and the control effort should be updated right away.


}

%\bibliographystyle{unsrt}
%\bibliography{references,ref2,ref3,haibo,shield}
 
