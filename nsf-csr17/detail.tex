\begin{table*}[tbp]
\scriptsize
  \centering
  \caption{Idempotent processing comparison}
  \label{Table: compare}
  \begin{tabular}{|l|c|c|c|c|c|c|}
    \hline
    \textbf{Solution} & \textbf{require RF protection} & \textbf{allow anti-dependence} &\
    \textbf{protect all region} & \textbf{degree of idempotence} & \textbf{runtime overhead} \\
    \hline
    \hline
    Idem\cite{de2012static} & YES & NO & YES &  Weak &MODERATE\\
    \hline
    Encore\cite{feng2011encore} & YES & YES & NO &  Weak & MODERATE\\
    \hline
    Quick  & NO & YES & YES &  Strong  & LOW \\
    \hline
    Quick with HW support  \&  & NO & YES & YES &  Strong  & NEGLIGIBLE \\  
    
    %& &     &    &  NO  &  \\        
    \hline
  \end{tabular}
\end{table*}

\ignore{
The remaining part of this section focuses on 
\begin{itemize}
\item How to relax the definition of traditional idempotent processing
to a more general one.
\item How to protect the liveness period of the inputs to the regions
before the entry to the regions at low cost.
\end{itemize}
}

\section{Quasi-idempotent Processing}
\label{sec:quasi}
Before discussing the details of Quick, this section first defines an
idempotent processing taxonomy.
The Figure~\ref{fig:texonomy} shows the comparison among the categories and highlights our
proposal: Quick (QUasi-Idempotent processing with last-value ChecKpointing).

\noindent\textbf{Idempotence }
A code region is idempotent if and only if (1) it can always recover its
inputs to the region entry (2) without a rollback. Note that 
since the idempotence is a software characteristic, it should remain the same
regardless of underlying hardware.
For example, it should not be affected no matter how the hardware is vulnerable to errors.

\noindent\textbf{Weak idempotence }
A code region is weak idempotent if and only if (1) it cannot always recover its
inputs to the region entry (2) without a rollback. If only the first condition
of the idempotence is relaxed, it becomes weak idempotence. For example, Idem~\cite{de2012static}
 only provides a weak idempotence since it works on the hardware equipped
 with the RF protection.  Note that Idem satisfies the first condition. Since
 Idem allocates a separate storage (i.e., pseudo-register) for the inputs with anti-dependence with register
 renaming, it does not need to restore anything through runtime.


\noindent\textbf{Quasi-idempotence }
A code region is quasi-idempotent if and only if (1) it can always recover its
inputs to the region entry (2) with a rollback. If only the second
condition of the idempotence is relaxed, it becomes quasi-idempotence.
Note that quasi-idempotence is not weak but supports a strong idempotence.
The goal of Quick is to achieve the quasi-idempotence at low lost.
This paper found out that Idem's register renaming often leads to more register
spilling in which the accesses to pseudo-registers (i.e., the inputs with
anti-dependence) turn into memory accesses.
For this reason, Quick does not allocate a new storage for the inputs.
Instead, it leverages the rollback runtime that takes over
time- and space-consuming work necessary for the recovery.

\noindent\textbf{Weak Quasi-idempotence }
A code region is weak quasi-idempotent if and only if (1) it cannot always recover its
inputs to the region entry (2) with a rollback. If the two conditions of
the idempotence are both relaxed, it becomes weak-quasi idempotence. For
example, Encore \cite{feng2011encore} only provides a weak quasi-idempotence.

\begin{figure}[t!]
\centering
%\begin{minipage}[b]{\columnwidth}
\centerline{\includegraphics[width=\columnwidth,angle=0]{figure/texonomy.pdf}}
\centering
\caption{\protect The taxonomy of idempotent processing: (a) true idempotence
processing, (b) Quick (this paper), (c) Idem~\cite{dekruijf12,idem12,idem13}, (d)
Encore~\cite{feng11}.
}
\label{fig:texonomy}
\end{figure}


\ignore{
Feng \emph{et~al.} \cite{feng2011encore}
proposes the similar idea of checkpoint the live-in variables which
have anti-dependence (i.e. Write-After-Read) at the entry of the non-idempotent
regions. Although they generalize their fine-grained recovery technique
like quasi-idempotence, they fail to protect the inputs' value before
the entry to the regions at low cost, and they limit their analysis
to interval analysis whose scope is single entry multiple exits (SEME) region.
}

%The goal of Quick is to achieve quasi-idempotent processing that can be applied to any type of region formation algorithms for the program. 
Table \ref{Table: compare} shows the differences between
the Quick and the state-of-the-art idempotent processing.
Both Idem \cite{de2012static} and Encore \cite{feng2011encore}
need the RF protection. Besides, Idem allows no anti-dependence on the inputs to
the region by register renaming while Encore and Quick
allow the anti-dependence. Furthermore, Encore
does not protect all the regions in the program to mitigate the runtime overhead caused by checkpointing. However, both
Idem and Quick can protect all the regions in the program.
%All the solutions do not require any hardware support except the Quick with hardware support. 
The runtime overhead of Quick is lower than that of competitors,
even if it provides a strong idempotence; Section~\ref{section:HW_support} shows
that simple hardware supports (Quick-Bypass and Quick-HWLog) can further reduce
the overhead significantly.

Note that Idem and Encore only provide a weak impotence without the RF
protection, since they start protecting the inputs with anti-dependence after the region
entry through register renaming or checkpointing.
In contrast, Quick preserves the inputs throughout their liveness period across the
region entry so that it doesn't need to rely on the RF protection.
For those inputs that have a flow dependence (i.e, Read-After-Write dependence)
across the region entry, Quick checkpoints their value as soon as they are
defined, i.e., at the beginning of their liveness period.
In a sense, Quick checkpoints them in a eager manner.

However, Quick checkpoints only one time for each input updated by tracking the
last write; in a sense, Quick's checkpointing has a lazy nature too.
That is, even if an input is defined multiple times, Quick
checkpoints only one time the value used for the last definition.
In this way, Quick can effectively protect the inputs to the region during their
entire liveness period.
Once a fault is detected during the execution of the region, the runtime system
takes a control to restore the checkpointed value.
Then, the program control goes back to the entry of the region and re-executes it
to recover the fault.

\begin{figure}[htbp]
%\centering
\includegraphics[scale=0.8]{figure/idem_compare.pdf} 
\caption{Quasi-idempotent processing region inputs vulnerable period comparison.}
\label{fig:Idem_vulnerable} (a) original program. (b) program transformed
by Idem\cite{de2012static}. (c) program transformed by Encore \cite{feng2011encore}.
(d) program transformed by Quick
\end{figure}

Figure \ref{fig:Idem_vulnerable} compares the period of idempotent processing
schemes which is vulnerable to soft errors without the RF protection.
Every block in the graph represents
a region. As represented in the Figure \ref{fig:Idem_vulnerable} (b), although Idem can successfully
avoid the anti-dependence on the input through register renaming, the input is
vulnerable to soft errors during the entire liveness period.
In Figure \ref{fig:Idem_vulnerable} (c), Encore can provide the input with
the protection only after the region entry. In particular, the input is vulnerable
during its liveness period before the region entry.
In contrast, Quick can effectively protect the inputs during the entire liveness
period as shown in Figure \ref{fig:Idem_vulnerable} (d).

To sum up, Quick can effectively checkpoint the inputs from their
last update with low overhead while keeping the RF free of expensive
protection schemes.
This paper further illustrates the compiler analysis of Quick in the section \ref{sec:QuickAlgorithm}
and proposes light-weight hardware supports in section \ref{section:HW_support}. 



\section{Quick} \label{sec:QuickAlgorithm}
In order to restore the original inputs to the region,
Quick need to first identify the proper set of the variables whose values should
be checkpointed (Section \ref{subsection:Identify CP set}).
Then, it needs to guarantee that two neighboring checkpoints never stores data
Before going into any further discussion, in order to make it clear to the
readers, this paper defines some terms that are intensively used throughout
the remaining part of this section as follows:
n the same memory location (Section \ref{subsection: SymInst Insert}).


%Section \ref{subsection:Identify CP set} illustrates the static analysis
%to generate proper checkpoint set while section \ref{subsection: SymInst Insert}
%proposes a simple technique to enable control-flow insensitive checkpointing
%so that neighboring checkpoint memory locations will never be in the same one.

\noindent\textbf{Quasi-Idempotent Regions ($\mathbb{R}$): } They are those
regions that are formed by any idempotent region formation algorithm.
Once the algorithm partitions a control flow graph (CFG),  the resulting
sub-graphs are considered as different quasi-idempotent regions. 

\noindent\textbf{Quasi-BasicBlocks: }
They are the same as the original basic block used by compilers.
In particular, the original basic block might be divided by the idempotent
region entry to break the anti-dependence, forming forming finer-grain basic blocks, i.e., Quasi-BasicBlocks.

\noindent\textbf{Region Live-In set ($\mathbb{RLI}$): } Region live-in set includes all the
inputs to the region, i.e., they have a flow dependence across the region entry.

%\noindent\textbf{Live-Out set ($\mathbb{LO}$): } Live-out set holds
%the variables in the basic block which are live-out to the basic block.
%A variable is considered live-out if and only if the variable is live-in
%to the basic block and it is not overwritten till the end of the basic
%block. 

\noindent\textbf{Definition set ($\mathbb{DEF}$): } This set set simply contains
the variables that are defined in the basic block.

\noindent\textbf{Checkpoint set ($\mathbb{CP}$): } Checkpoint set refers to
all the variables that need to be checkpointed in the definition set of the
basic block.  If a variable is in the checkpoint set of the region, it will be
checkpointed right after the last write to the variable in the basic block.

\begin{algorithm}[htb]
\caption{Checkpoint Set Generation Algorithm}
\label{CPAlgorithm}
\begin{algorithmic}[1]
\Require $\mathbb{R}$, $\mathbb{RLI}$, $\mathbb{DEF}$
\Ensure $\mathbb{CP}$
\State  IN$[exit] \gets \emptyset$ \label{Initialization_begin}
\State {// Initialize the IN set of each basic block}
\For{each Quasi-BasicBlock $bb_i$ other than $exit$}
  \If {$r \in \mathbb{R}$ start from $bb_i$}
    \State IN$[bb_i] \gets \mathbb{RLI}[r]$
  \Else
    \State IN$[bb_i] \gets \emptyset$
  \EndIf
\EndFor \label{Initialization_end}

\While {Change to any IN$[bb_i]$ occurs} \label{dataflowbegin}
\For{each Quasi-BasicBlock $bb_i$ other than $exit$}
  \State OUT$[bb_i] \gets \bigcup_{bb_j \in succ of bb_i}$IN$[bb_j]$
  \label{OUT}
  \State IN$[bb_i] \gets$ IN$[bb_i] \bigcup$  (OUT$[bb_i]$ - $\mathbb{DEF}$$[bb_i]$)
  \label{IN}
  \State $\mathbb{CP}[bb_i] \gets$ OUT$[bb_i] \bigcap \mathbb{DEF}$$[bb_i]$ 
  \label{CP}
\EndFor
\EndWhile \label{dataflowend}
\end{algorithmic}
\end{algorithm}


\subsection{Identifying Checkpoint Set} \label{subsection:Identify CP set}
\paragraph{}

\noindent This paper performs a simple backward data flow analysis to generate the checkpoint set.
%To begin with, we assume the original program is divided into
%a sets of regions, $\mathbb{R}$. For each region, $\mathnormal{r}$ $\in$ $\mathbb{R}$ , we
%calculate its region live-in set, $\mathbb{RLI}$[$\mathnormal{r}$],
%which contains all the upward exposed variables whose values are used prior to
%any definition of the variables through pre-order depth-first-search traversal of the region.
%%It is easy to get the definition set $\mathbb{DEF}$[$\mathnormal{bb}$] for
%%each basic block, by just recording the variables that are defined in this basic block.
%After getting the $\mathbb{RLI}$[$\mathnormal{r}$] and $\mathbb{DEF}$[$\mathnormal{bb}$] 
%set, we can apply them to our static data flow analysis to generate the checkpoint
%set. For the ease of illustration, we assume all the region will start from the
%beginning of the basic block and we ignore the surrounding details around the regions.

%Since the region live-in set and basic block definition set are generated,
%the proposed data flow analysis can be formulated to generate the checkpoint set. 
As shown in Algorithm \ref{CPAlgorithm}, we first initialize
the IN set of the each basic block through line \ref{Initialization_begin} to
line \ref{Initialization_end}. If a region starts from one of
the basic blocks, its region live-in set is assigned to the basic block.
To eliminate the anti-dependence, a region may start from the middle of the
basic block or several regions start in the basic block. 
In these cases, the basic block is divided into quasi basic blocks. 
%where only one region will start from the beginning of the each the quasi basic block. 
%We assume at most one region starts from one basic block in Algorithm \ref{CPAlgorithm} for better understanding.

\begin{figure*}[htbp]
\centering\includegraphics[scale=0.5]{figure/identify_CPSet.pdf} 
\caption{Static analysis of quasi-idempotent processing program.}
\label{fig:identify_CPSet} (a) original program which is divided into 4
regions in different color shadow.
(b) static analysis of original program.
(c) ultimate checkpoint location set(indicated as CP variable name).
\end{figure*}

The transfer function from line \ref{OUT} to line \ref{IN}
indicates how IN and OUT sets are generated for each basic block.
The OUT$[bb_i]$ set is the sum of In set of $bb_i$'s successor basic blocks
while IN$[bb_i]$ is the sum of elements in IN$[bb_i]$ and OUT$[bb_i]$ except for
the variables in the $\mathbb{DEF}[bb_i]$ set.
This algorithm keeps iterating until a fix point is reached.
After that, the checkpoint set, $\mathbb{CP}$, for each basic block is
generated as shown in line \ref{CP} which is the intersection of $\mathrm{OUT}[bb_i]$ set and
$\mathbb{DEF}[bb_i]$ set of the basic block.
If the checkpoint set is not empty (i.e.\ $\mathbb{CP}[bb_i] \not= \emptyset$),
we need to checkpoint the value of the variables in the set right after the last write of the variables. 

Figure \ref{fig:identify_CPSet} depicts how the data flow analysis
generates the proper checkpoint set.
In the Figure, each box represents o basic block. Those basic blocks in the same
color shadow indicate a region they belong to..
In Figure \ref{fig:identify_CPSet} (b), the labels
beside every basic block represents the final outcome of
the data flow analysis shown in Algorithm \ref{CPAlgorithm}.
Figure \ref{fig:identify_CPSet} (c) shows the resulting checkpoints inserted by
our compilers.

In addition, we need to protect those arguments passed to a function.
This is necessary to re-execute the function when a fault is occurred in the
very first region of the function.
To this end, Quick identifies those registers used as an function argument according to
the calling convention. In the same way, Quick then checkpoints their values as soon as they are defined.
\ignore{
%their calling convention style (e.g.\ X86-64 \cite{matz2013system} style calling convention).

%For each call site in the program, we decide to
%protect the variables which are the caller-saved arguments to
%the function (which might be used later across the entry to
%the fuction). We checkpoint these variables in the same
%As-Soon-As possible fashion by performing reverse reorder
%depth-first-search. 
}
We also protect the return variable of the function call by checkpointing for a
similar reason.


%after the call site through considering
%the CALL instruction defines these return variables in
%the corresponding basic blocks.

%The proposed checkpoint ASAP technique applies the above
%analyse to generate the proper checkpoint location set, $\mathbb{CP}$.
%Now, the memory location allocation should come into play
%as illustrated in section \ref{subsection: SymInst Insert}.

\begin{figure*}[htbp]
\centering\includegraphics[scale=0.55]{figure/MemAlloc.pdf} 
\caption{Checkpoint Memory Location Allocation and Symmetric Instruction Insertion.}
\label{fig:MemAlloc} (a) original program colored by Depth first search algorithm
(red arcs indicate conflict paths of variable Y)
(b) symmetric instructions insertion under depth-first search order.
(c) symmetric instructions insertion using path profiling information.
\end{figure*}




\subsection{Checkpoint Memory Location Allocation \&
Symmetric Instuction Insertion}\label{sec:symmetric}
\paragraph{}

\noindent To restore the original inputs to the region on a fault, Quick
reloads the checkpointed value in memory for the recover. Thus, it has to guarantee
that the previous checkpointed value in an earlier region is not corrupted by
the current region, i.e., its checkpoint should not overwrite the value.
With this mind, Quick tries to allocate different memory location to neighboring
checkpoints.
%The implication is that Quick  needs to protect the inputs to the region  before the entry of current value from being corrupted by the current region. 
%Note that the same variable can be defined across the regions, and therefore we
%should not checkpoint the values of such a variable to the same memory
%location. 
%That is because the previously checkpointed data will be overwritten if the current region. 
%For example, when a fault is detected during the process of this region, we cannot recover the original
%inputs' value as it is already overwritten by the checkpoint instruction
%in the region. 

%Therefore, we have to protect the checkpointed value through
%allocating memory locations for each checkpoint instruction for each variable.

%In a conservative way, the neighboring checkpoint locations should not
%share the same memory location in order to guarantee the neighboring region
%will not checkpoint to the same memory location for the same variable.
%This is conservative because every region in the program will only
%have at most one checkpoint instruction for the same variable along
%one path and the neighboring memory locations can be distributed into
%two region which are not neighboring.
%In this way, we are able to effectively address the dilemma of
%overwriting the previous checkpointed value.
Essentially, the problem of allocating different memory locations to
neighboring checkpoints can be reduced to a 2-coloring problem.  %for the neighboring %checkpoint memory locations along one path.
Coloring the memory location for each checkpoint for each variable can be easily
achieved by performing a pre-order depth-first search. 
During the search, each color (i.e., different memory location) can be determined. 
However, the memory locations of two neighboring checkpoints could not be
colored with different color, which might be due to control flow divergence.
%To achieve the coloring, we have to guarantee that all the colors of
%the incoming paths to a joint point should always be the same.

This paper proposes a simple way to deal with the problem of the control flow divergence. 
We selectively insert additional checkpoint instructions to the control flow
paths to make sure that all the colors from incoming paths share the same color. 
We call the dummy instructions as symmetric instructions. They  are inserted in
the original program right after the conflicting region and form
a new region comprised of one or more symmetric instructions to different
variables.

As shown in Figure \ref{fig:MemAlloc} (a), we color each checkpoint instruction
with either asterisk or pound sign in front of the checkpoint instructions
for each variable.
First, we color the checkpoint instruction using a pre-order depth-first
search along one path. Then, we continue to color the other path.
However, a conflict happens when we try color the other path as indicated
by the red arrow in Figure \ref{fig:MemAlloc} (a). Here the two neighboring
checkpoints are assigned the same location (*) for a variable, Y.
In order to deal with the conflict, we immediately insert a symmetric instruction
after the region in the green shadow and create a new region, i.e., the purple
shadow in Figure \ref{fig:MemAlloc} (b). As a result, the conflict is resolved
since the symmetric instruction checkpoints Y to a different location (\#), i.e., 
 when a fault occurs during the execution of the bottom region, Quick can 
 restore Y by reloading the original value from the memory location (\#).

In general, the overhead of the inserted symmetric instructions are not that
significant. However, there might be some performance degradation if the
symmetric instructions are inserted to a hot path.
In that case, Quick can leverage path profiling information so that it can
selectively inserts the symmetric instructions on a cold path.
Figure \ref{fig:MemAlloc} (c) shows such an example of inserting
the symmetric instruction to the cold path.

%\input{recovery}

