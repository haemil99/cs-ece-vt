import matplotlib.pyplot as plt
from matplotlib.ticker import LogLocator, FixedLocator, ScalarFormatter
import numpy as np
#p = np.array([10, 20, 30, 40, 50,60, 70, 80, 90, 100, 120, 140, 160, 200, 300, 400, 500, 1000])
freq = np.array([0.5, 0.8, 1, 1.2, 1.5, 2, 2.5])
ax0ticks = np.array([1, 10, 20, 40, 100, 200, 1000])
ax1ticks = np.array([10, 100, 200, 400, 800, 2000, 10000])
ax2ticks = np.array([100, 500, 1000, 3000, 10000, 10000])
ax3ticks = np.array([100, 1000, 2000, 10000, 30000, 100000])
die_size0 = 0.45

def f(t,s,f):
    return (np.sqrt(s/(2*2*t)))*100*f
def f2(t,s,f):
    return (np.sqrt(s/(2*4*t)))*100*f
def thresDet(thres,s):
    return np.ceil((thres*s)/0.0005)
#def thresDet2(thres,s):
#    return np.ceil((thres*s)/0.0005)*4

fig, ((ax0, ax1, ax2, ax3), (ax4, ax5, ax6, ax7)) = plt.subplots(nrows=2, ncols=4)
#plt.plot(p, f(p), 'o-')
def plot_gragh(ax,p, size, ticks, title):
  f05, = ax.plot(p, f(p,size,0.5))
  f08, = ax.plot(p, f(p,size,0.8))
  f10, = ax.plot(p, f(p,size,1))
  f12, = ax.plot(p, f(p,size,1.2))
  f15, = ax.plot(p, f(p,size,1.5))
  f20, = ax.plot(p, f(p,size,2))
  f25, = ax.plot(p, f(p,size,2.5))
  num01 = thresDet(0.01, size)
  num02 = thresDet(0.02, size)
  num05 = thresDet(0.05, size)
  num10 = thresDet(0.1, size)
  num20 = thresDet(0.2, size)
  l1, = ax.plot(np.repeat(num01, 7), f(num01, size, freq), "k*")
  l2, = ax.plot(np.repeat(num02, 7), f(num02, size, freq), "ks")
  l5, = ax.plot(np.repeat(num05, 7), f(num05, size, freq), "k^")
  l10, = ax.plot(np.repeat(num10, 7), f(num10, size, freq), "kx")
  l20, = ax.plot(np.repeat(num20, 7), f(num20, size, freq), "ko")
  ax.annotate("%d"% num01, xy=(num01, f(num01, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points', fontsize=20)
  ax.annotate("%d"% num02, xy=(num02, f(num02, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points', fontsize=20)
  ax.annotate("%d"% num05, xy=(num05, f(num05, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points', fontsize=20)
  ax.annotate("%d"% num10, xy=(num10, f(num10, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points', fontsize=20)
  ax.annotate("%d"% num20, xy=(num20, f(num20, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points', fontsize=20)
  ax.legend( (f25, f20, f15, f12, f10, f08, f05, l1, l2, l5, l10, l20),
              ("2.5$Ghz$/%s$mm^2$"% size,'2$Ghz$/%s$mm^2$'% size,'1.5$Ghz$/%s$mm^2$'% size,
               '1.2$Ghz$/%s$mm^2$'% size,'1$Ghz$/%s$mm^2$'% size,
               '0.8$Ghz$/%s$mm^2$'% size, r'0.5$Ghz$/%s$mm^2$'% size,
               '1% Overhead', '2% Overhead', '5% Overhead', '10% Overhead', '20% overhead'),
               loc='upper right', shadow=True, numpoints=1, prop={'size':12})
  ax.set_ylabel('Detection latency (cycles)', fontsize=14)
  ax.set_xlabel('# of detectors', fontsize=14)
  ax.set_xscale('log')
  #ax.set_xticks([10, 60,  120, 200, 300, 400, 500, 1000])
#  ax.set_xticks([10, 20, 30, 40, 50, 70, 100, 150, 200, 300, 400, 600, 1000])
  ax.set_xticks(ticks)
  ax.set_title('(%s)'% title , horizontalalignment='center',
               verticalalignment='bottom',y=-0.17,fontsize=18)
  ax.get_xaxis().set_major_formatter(ScalarFormatter())

def plot_gragh2(ax,p, size, ticks, title):
  f05, = ax.plot(p, f2(p,size,0.5))
  f08, = ax.plot(p, f2(p,size,0.8))
  f10, = ax.plot(p, f2(p,size,1))
  f12, = ax.plot(p, f2(p,size,1.2))
  f15, = ax.plot(p, f2(p,size,1.5))
  f20, = ax.plot(p, f2(p,size,2))
  f25, = ax.plot(p, f2(p,size,2.5))
  num01 = thresDet(0.01, size)
  num02 = thresDet(0.02, size)
  num05 = thresDet(0.05, size)
  num10 = thresDet(0.1, size)
  num20 = thresDet(0.2, size)
  l1, = ax.plot(np.repeat(num01, 7), f2(num01, size, freq), "k*")
  l2, = ax.plot(np.repeat(num02, 7), f2(num02, size, freq), "ks")
  l5, = ax.plot(np.repeat(num05, 7), f2(num05, size, freq), "k^")
  l10, = ax.plot(np.repeat(num10, 7), f2(num10, size, freq), "kx")
  l20, = ax.plot(np.repeat(num20, 7), f2(num20, size, freq), "ko")
  ax.annotate("%d"% num01, xy=(num01, f2(num01, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points',fontsize=20)
  ax.annotate("%d"% num02, xy=(num02, f2(num02, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points',fontsize=20)
  ax.annotate("%d"% num05, xy=(num05, f2(num05, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points',fontsize=20)
  ax.annotate("%d"% num10, xy=(num10, f2(num10, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points',fontsize=20)
  ax.annotate("%d"% num20, xy=(num20, f2(num20, size, 2.5)), xycoords='data',
              xytext=(3, 3), textcoords='offset points',fontsize=20)
  ax.legend( (f25, f20, f15, f12, f10, f08, f05, l1, l2, l5, l10, l20),
              ("2.5$Ghz$/%s$mm^2$"% size,'2$Ghz$/%s$mm^2$'% size,'1.5$Ghz$/%s$mm^2$'% size,
               '1.2$Ghz$/%s$mm^2$'% size,'1$Ghz$/%s$mm^2$'% size,
               '0.8$Ghz$/%s$mm^2$'% size, r'0.5$Ghz$/%s$mm^2$'% size,
               '1% Overhead', '2% Overhead', '5% Overhead', '10% Overhead', '20% overhead'),
               loc='upper right', shadow=True, numpoints=1, prop={'size':12})
  ax.set_ylabel('Detection latency (cycles)', fontsize=14)
  ax.set_xlabel('# of detectors', fontsize=14)
  ax.set_xscale('log')
  #ax.set_xticks([10, 60,  120, 200, 300, 400, 500, 1000])
#  ax.set_xticks([10, 20, 30, 40, 50, 70, 100, 150, 200, 300, 400, 600, 1000])
  ax.set_xticks(ticks)
  ax.set_title('(%s)'% title , horizontalalignment='center',
               verticalalignment='bottom',y=-0.17,fontsize=18)
  ax.get_xaxis().set_major_formatter(ScalarFormatter())

plot_gragh(ax0,np.arange(1,1001,1), 0.45, ax0ticks, 'a')
plot_gragh(ax1,np.arange(10,10001,10), 4.6, ax1ticks, 'b')
plot_gragh(ax2,np.arange(100,9991,50), 15.0, ax2ticks, 'c')
plot_gragh(ax3,np.arange(100,100001,100),  30.0, ax3ticks, 'd')
plot_gragh2(ax4,np.arange(1,1001,1), 0.45, ax0ticks, 'e')
plot_gragh2(ax5,np.arange(10,10001,10), 4.6, ax1ticks, 'f')
plot_gragh2(ax6,np.arange(100,9991,50), 15.0, ax2ticks, 'g')
plot_gragh2(ax7,np.arange(100,100001,100),  30.0, ax3ticks, 'h')
plt.show()

#import matplotlib.pyplot as plt
#import numpy as np
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
#
#majorLocator   = MultipleLocator(20)
#majorFormatter = FormatStrFormatter('%d')
#minorLocator   = MultipleLocator(5)
#
#
#t = np.arange(0.0, 100.0, 0.1)
#s = np.sin(0.1*np.pi*t)*np.exp(-t*0.01)
#
#fig, ax = plt.subplots()
#plt.plot(t,s)
#
#ax.xaxis.set_major_locator(majorLocator)
#ax.xaxis.set_major_formatter(majorFormatter)
#
##for the minor ticks, use no labels; default NullFormatter
#ax.xaxis.set_minor_locator(minorLocator)
#
#plt.show()
