\subsection{Task 1: Data-Consistency-Aware Lightweight Distibuted Checkpointing}
\label{sec:task0}

%\begin{figure}[t!]
\begin{wrapfigure}{L}{0.4\textwidth}
\centering
\centerline{\includegraphics[width=0.4\columnwidth,angle=0]{figure/Store_gurad_buffer.pdf}}
\centering
\caption{\protect The high-level view of Sfork hardware scheme}
\vspace{-0.2cm}
\label{fig:verify-hw}
\end{wrapfigure}

Low-cost verification and recovery of executed program regions is crucial for achieving soft error resilience in a way that
does not hurt performance and power efficiencies.
In light of this, Sfork leverages a simple hardware module to realize the
region verification and the recovery for detected soft errors.
As described in Figure~\ref{fig:verify-hw}, the hardware module interacts with
the ROB and the gated store buffer (GSB) to recognize the boundary of executed
regions and to write back their verified data, respectively.
To keep track of those regions that have not been verified yet, 
the hardware module leverages the region boundary buffer (RBB) whose entry is
allocated whenever the boundary instruction is executed.
The RBB entry is a tuple of three values: (1) the PC of the boundary
instruction; once the region ending at the boundary is verified, the PC of the boundary
instruction is used
as a recovery point on an error detected, (2) the tail pointer of GSB; this is
necessary to write back the data written in the region once it is verified, 
(3) the execution cycles of the region
%; this is important information for Sfork's efficient region verification as described in Section~\ref{sec:rbb}.
Whenever a new region is verified, the hardware module writes the PC of the boundary
instruction that ends the region, to a special register called RP (recovery PC) which
will be used in case of an error.

Leveraging RBB, we propose a timer based approach to efficiently achieve region verification based on the
following insight.

\newtheorem{axiom}{Axiom}
\renewcommand{\baselinestretch}{1.0}
\begin{axiom} \label{ax:insight}
If a given n'th region, $R_n$, is verified at a time $T$, then the very next region,
$R_{n+1}$, will be verified at later time $T'$, $T' = T + ElapsedTime(R_{n+1})$ where $ElapsedTime$ maps a region to its execution cycles.
\end{axiom}

As shown in Figure~\ref{fig:verify-hw}, Sfork thus leverages a watchdog timer called $ToWait$ which represents
the number of cycles for which the region in the head entry of RBB should wait more before being verified.
On the other hand, Sfork maintains an auxiliary counter called $HasWaited$ which represents
the number of cycles for which the region has already waited to be verified.

When a region boundary instruction is executed, Sfork allocates a new RBB entry
for the region that has just finished at the boundary.  At the same time, Sfork
reads the timer ($ToWait$) and the counter ($HasWaited$) to calculate the
elapsed time of the region, i.e., $RegionTime$ (See Figure~\ref{fig:verify-hw}.  Note that unlike the watchdog
timer that automatically counts down each cycle, Sfork here reads the value of
$HasWaited$ counter written at the previous region boundary.  Therefore, the
$RegionTime$ can be obtained by subtracting the value of $HasWaited$ from the
error detection latency ($DL$).  Then, Sfork records the resulting $RegionTime$
in the new RBB entry allocated, and updates the $HasWaited$ counter as '$DL -
ToWait$'.

When the watchdog timer expires, i.e., $ToWait$ becomes zero, Sfork pops the entry
in the head of RBB since the corresponding region is now verified. Thus, the PC existing
in the entry is written in RP that will be used for error recovery.
Then, GSB marks those entries preceding the GSB pointer of popped RBB entry as
verified, so that they can be written back to L1 data cache.
And then, the watchdog timer is reset to the $RegionTime$ of a new head
entry of RBB (See the Axiom~\ref{ax:insight}). 
In particular, the value of $RegionTime$ is subtracted from $HasWaited$ to
update it. Recall that $HasWaited$ means the time for which the region in the
head entry of RBB has waited to verify itself. Therefore, excluded is the time the region had
waited to verify the very previous region, which is just verified and the popped from RBB.

If an error is detected, Sfork's recovery handler discards every
unverified data in GSB and empties the RBB.  
Then, Sfork restores the
checkpointed register values from memory. At this point, all the program status is
guaranteed to be the same as it were before the error occurs. 
Finally, Sfork jumps back to the most recently verified region whose address is
available in RP, the recovery point register.



\begin{figure*}[thbp]
\centering
\begin{minipage}[b]{0.9\textwidth}
\centerline{\includegraphics[width=0.9\textwidth,angle=0]{figure/store_guard_example.pdf}}
\end{minipage}
\centering
\caption{\protect Dynamic Store Guard Example}
\vspace{-0.2cm}
\label{fig:sbexample}
\end{figure*}


Figure~\ref{fig:sbexample} illustrates a simple example for better understand how RBB verification works.
Along the timeline, the blue arrow corresponds to the time points on which a region
boundary instruction is executed, while the orange arrow corresponds to the time points on which a
region is verified, and has the name within the parenthesis above the arrow.
When a region boundary instruction is executed, i.e., a blow arrow is reached in
Figure~\ref{fig:sbexample}, the RBB is updated with the tuple above the arrow comprised of 
the PC of the boundary instruction, the tail pointer the GSB, and $RegionTime$ shown as RT
in Figure~\ref{fig:sbexample}.
Below the timeline, there are numbers which represent the timer value at each time point. 
When the timer expires reaching 0, RBB first pops out
the head entry and updates the timer with the $RegionTime$ in the updated head entry.
Below the timer, there are three tables representing the status of RBB at different time
points, i.e., t1, t4, and t6.
As we can see, at t1, there are two region entries sitting in the
table waiting to be verified. At t4, the region r1 has already been verified since t2.
Therefore, the head pointer of RBB comes to point to r2 at t4.
At t3, a soft error happens and it is detected at t6 after the error detection
latency. At t6, r2 has already be verified since t5 and been popped out from the RBB, which means
the instructions before r2 has been verified. Therefore, at t6, Sfork redirects program
control to the end of the verified region (r2 in this case), thus re executing r3 from the
beginning.  The takeaway is that Sfork can achieve the region verification by simply
tracking the execution cycles of each executed region.

\paragraph{Resilience for Multithreaded Execution} It is important to note that Sfork's
verification technique works for multithread applications at low cost.  In general,
achieving soft error resilience for multithreaded execution is considered as a much more
difficult problem. The reason is that even if one thread can detect an error and recover
from it, there could be other threads that might read corrupted value due to the error. In
this case, the recovery process is very challenging possibly requiring a global
verification point across multiple threads.  We argue that without worrying such a complex
issue, Sfork can make multithreaded applications robust against soft errors based on the
same way used for sequential applications. The idea is that Sfork never allows other
threads to read unverified data, since it is kept in the GSB. That is, other threads might
end up reading old value of the data that has been verified before, but this is completely
valid operation under a TSO memory consistency model.  One relevant topic  we will
investigate carefully is the way to deal with fence instructions. By default, a fence
instruction forces all the stores kept in the GSB to be cleared (flushed out). It breaks
the verification process of Sfork, thus possibly generating SDC. Our plan to cope with this
problem will remain the same, i.e., we will delay the GSB clear operation until the most
recently committed store there at the moment of the fence request is verified. This means
that the maximum delay time should not be greater than the error detection latency which
is expected to be insignificant.  In case of scenario where a fence instruction is
frequently executed, we will also explore some speculation techniques that can avoid the
delay of the fence at the expense of misspeculation cost which would be paid once in a
while.

\paragraph{Related Work}

Checkpointing the program states (memory and register) enables a program to recover from
an arbitrary fault by rolling back the program to a previous safe checkpoint and
re-execute the program from that point~\cite{wang2006restore, khudia2012efficient,
feng2010shoestring}. However, taking a snapshot of full program states often results in
performance loss and high power consumption.  Researchers have proposed techniques such as
concurrent checkpointing~\cite{Li94concckpt} that leverages the memory protection hardware
to continue the execution of the original process while its checkpoint is being saved; and
incremental checkpointing~\cite{Plank95libckpt} that reduces the set of data to checkpoint
by avoiding rewriting portions of the process states that do not change between
consecutive checkpoints.  However, software-based full checkpointing solutions are still
slow, and hardware solutions are not complexity-efficient as it requires duplications of
microarchitectural states such as RF (register file) and RAT, which Sfork does not
require.
As an alternative, flushing
the pipeline to recover from the soft error \cite{racunas2007perturbation,
dimitrov2007unified} have been proposed. This approach is expected to be very efficient in
term of runtime overhead. However, this approach is based on the assumption that detection
can be done within the pipelining instruction window, and thus cannot guarantee recovery
if a soft error has been latent for a while. Moreover, the approach cannot recover if the
written values in RFs gets corrupted due to soft errors, thus requires ECC-like protection
in RFs, which comes with performance and power penalty. In contrast, Sfork achieves 100\%
soft error recovery and provides a very low hardware cost mechanism, e.g., Sfork-enabled processor core does not need even ECC
which is required in almost all existing work.





\paragraph{Preliminary Work}


\begin{wrapfigure}{R}{0.4\textwidth}
\includegraphics[width=0.4\textwidth,angle=0]{figure/gsb2.pdf}
\centering
\small
  \caption{The overhead of GSB}
  \label{fig:gsbs}
  \vspace{-0.4cm}
\end{wrapfigure}


We have started implementing a basic feature of the gated store buffer (GSB) on top of
GEM5, a cycle-accurate architecture simulator, and have focused mainly on evaluating the
performance impact of holding the stores in the GSB across different
worst-case soft error detection latencies.
Our preliminary experiments have led to a very encouraging result.  
Figure~\ref{fig:gsbs} shows the execution time overhead caused by GSB compared to the
original case that uses a normal store buffer for several applications from MediaBench suite~\cite{mediabench}.
Here, the store buffer size is 42, which follows that of Intel Haswell
processors.
It turns out that the performance impact of
GSB is negligible, i.e., less than 1\%. 
Such a low overhead is consistently observed when different error detection latencies are
used.  For example, when the latency is 30 cycles, the performance overhead is around 0.5\%.
Consequently, we expect that Sfork can be leveraged to achieve complete soft error
resilience at low cost for production systems.
The only source of overhead Sfork might cause would be the checkpoint
instructions inserted by our compiler. The Task 3 addresses the checkpointing overhead
with compiler and architectural optimization techniques.



\paragraph{Open Question}
To eliminate silent data corruption (SDC), the Sfork's GSB should not release unverified
stores to memory.  However, in reality the capacity of GSB is limited, and therefore Sfork
can cause pipeline stalls when the GSB is full and all the stores there have not been
verified yet.  Note that each stall time cannot be greater than the error detection
latency time after which some stores in GSB should be verified.  To this end, we will
explore different design options of GSB. Simply increasing the size of GSB might not work,
since it is implemented expensive CAM (content-addressable-memory) structure.  One
promising option would be to have a separate buffer to accommodate checkpointing stores,
letting the original GSB deal with only normal stores. This would significantly reduce the
store traffic to the GSB thereby mitigating pipeline stalls. Another benefit of doing this
is that the separate checkpointing store buffer does not require CAM structure for its
implementation, since the checkpointed value will not be accessed unless an error occurs.
As a result, we can increase the size of the buffer at our disposal under the power and
area budgets.
 

