\subsection{Task 2: Power Failure Safety Net for Forward Execution Progress Guarantee}
\label{sec:task2}
In IoBT devices, the frequency of power failures can be extremely high
especially when the quality of the energy harvesting power supply is very poor.
In such a case, the application may not even make a progress consuming all the
harvested energy for repetitive backup and recovery operations across power
failures during the execution of the same region.
Therefore, it is the most important challenge to guarantee forward execution progress.
%This section firsts demonstrates how the state-of-the-art techniques lead to
%stagnation, and proposes RockClimb's power failure safety net that always ensures
%forward progress.



\noindent{\bf Curse of Power Failure Period: }	
Suppose an idempotent region (referred to as region hereafter) whose execution time is greater than the the power failure
period, i.e., the time between the failures.
If they periodically occur with the same frequency, 
the program comes to rolls back to the beginning of the region again and again.
That is because the failures keep occurring before the end of the region is
reached, in which case the program just wastes energy in vain making no
forward execution progress. We call this phenomenon {\bf stagnation}. 
This can happen to all software/hardware
approaches~\cite{Lucia_2015,cleancut16,Xie_2015,Ransford_2011,Lui_2015,Wang_2012,Ma_2015}. 
\ignore{
By default, RockClimb is less vulnerable to 
such {\it stagnation} compared to 
the existing approaches.
The reason
is that they restore the entire register file whenever a region is re-executed for
recovery across power failures, thereby consuming much more energy.
On the contrary, RockClimb restores only a few live-in registers of the region
to re-execute it restarted for recovery as shown Figure~\ref{fig:reg_com} (b).
}

\noindent{\bf What about NVP with a Voltage Detector?: }	
\begin{wrapfigure}{R}{0.4\textwidth}
\centering
\centerline{\includegraphics[width=0.4\columnwidth,angle=0]{figure/nvp.pdf}}
\centering
\caption{\protect The NVP design~\cite {Wang_2012} that 
always try to secure enough energy in the capacitor ($C_{PL}$) to finish backup and recovery.}
\vspace{-0.2cm}
\label{fig:nvp}
\end{wrapfigure}
%
The only existing solution to get around the {\it stagnation} seems to leverage nonvolatile
processors (NVP)~\cite{Ma_2015,Wang_2012} equipped with a voltage detector shown in Figure~\ref{fig:nvp}. However,
they try to do this at the sacrifice of energy-efficiency. In other words, they always
suffer from the loss of time and energy due to the charging of their two configurable capacitors ($C_{PL}$
and $C_{VD}$ in Figure~\ref{fig:nvp}). One capacitor ($C_{PL}$) guarantees enough energy
for NVP to finish the backup using its hybrid register file (HFD) comprised of nonvolatile
storage attached to a standard CMOS D latches~\cite{Wang_2012}.  Note that the other
capacitor ($C_{VD}$) used for voltage monitoring is conservatively configured
for $V_{ref}$ to enable
NVP's backup/recovery in a safe way, which can be easily abused by the voltage trace
oscillating around $V_{ref}$ repeatedly performing HFD backups in vain.
Therefore, NVP can technically lead to the {\it stagnation} as well.
For the same reason, the other techniques
~\cite{hibernus2015,hinernus15b,hinernus16,Jayakumar:2015:QUR} that use the voltage detector
are vulnerable to the {\it stagnation} too.

\noindent{\bf Overcoming the Curse with RockClimb: }	
To ensure forward program execution, RockClimb leverages 
 the insight that {\it stagnation} occurs due to
the lack of energy to finish the problematic region. 
This brings a natural question; what if we are able to know how much energy is needed to finish the region being executed?
Then, we can wait for the capacitor to secure the necessary amount of the energy. Once it is
completely charged to the capacitor, we can simply execute the region with the guarantee of
no power failure! That is why we name this proposal RockClimb, being inspired by how people climb the rock;
when they keep failing to make a progress, they usually wait eating energy bars until they make sure to be able to move forward.
RockClimb is completely different from the existing power failure tolerance models that
blindly re-execute the {\it stagnated} 
task (DINO~\cite{Lucia_2015}, Chain~\cite{colin2016chain}, and
Alpaca~\cite{maeng2017alpaca}), loop body (Mementos~\cite{Ransford_2011}), or region
 (Ratchet~\cite{rachet16}), whenever power comes back after the failures.

\ignore{
Recall that the prior schemes all rely on centralized checkpointing (Table XX), i.e., they
read entire register file in the wake of power failures. 
Along with bulk NVM (FRAM in MSP430FR) stores,
Such consecutive NVM loads for regiser file restoration consume considerable energy (high power
and time) that othwerwise can be used for escaping the stagnation.
In contrast, RockClimb 
\CJ{Verify if the above is true}
}

\subsubsection{Task 2.1: FluidCap: Minimizing Capacitor Leakage}	

\begin{wrapfigure}{R}{0.4\textwidth}
    \includegraphics[width=0.4\textwidth]{figure/schem.png}
    \caption{7-bit Banked Capacitor}
    \label{fig:7bit}
\end{wrapfigure} 

We propose a novel digitally controlled and embedded on-die capacitor called FluidCap;
Min et al.'s research showed the validity of embedding MLCC style capacitors on the
physical CPU die ~\cite{Min2013}, so we can make the assumption that the CPU 
manufacturer would be able to implement such an embedded design in conjunction with
FluidCap.

The goal is to adjust the threshold of energy to be stored in the capacitor, according to the energy required to
finish each region.
We propose to encode the necessary energy to finish the region in the boundary instruction
at its beginning. We will also slightly modify the processor in two ways; (1) decoding
logic for the processor to figure out the necessary energy, and (2) control logic to pause the
processor until the capacitor secures the energy and to wake up the processor once
secured.
With the simple architectural support, RockClimb can achieve waste-free (rollback-free) execution. Note that this execution mode can completely eliminating {\it
stagnation}, thus providing the forward execution guarantee.

To the 
best of our knowledge, we are the first to propose such a 
digitally controlled on-die capacitor for energy-harvesting systems.  To control 
this capacitor, we also have a major contribution by using the WCE--Worst Case Energy--(Task 2.2) of partitioned program 
regions to direct the energy store of the system.
 
Because of the characteristics of current capacitors (as in non-ideal), they have adverse
properties such as leakage current and large area footprints and because of the
fundamental properties of electronics, charging times are impossible to eliminate.

For all of these cases, the one variable that exacerbates all of these problems is the 
capacitance of said capacitor. This can be seen by looking at the formula for leakage 
current of a ceramic capacitor \(I_{leakage} = IR * V_{applied}\) ~\cite{Horowitz2016}, 
where \(IR = X * C\), where \(X\) is a constant given by the manufacturer.  From
this, we can see that as capacitance goes up so does the leakage current.
In addition to the leakage current, both charge time \(\tau = C * R\) and area \(A = (d/n) * (C/\epsilon)\) ~\cite{Horowitz2016} are both proportional to capacitance.  So by adding
a large input capacitor blindly is problematic.

With that in mind, we propose a banked capacitor structure with each 
sub-capacitor controlled by a simple transistor.  The sub-capacitors are connected in 
parallel with the controlling transistors supplying this parallel connection. For example, 
Figure~\ref{fig:7bit} depicts a simplified schematic of a 7-bit system with
the connections being proposed. Here, C1 in this schematic should be the minimal 
capacitance of the system, 
with each C2-C8 being a very small capacitor, with their capacitances governed by theresolution of the bit-width of the system.

\begin{wrapfigure}{R}{0.36\textwidth}
\scriptsize
\begin{tabular}{lccl}
   & FluidCap's  & BulkCap's \\
 Application  & average  & required\\
    & \(n F\) & \(n F\) \\
\hline
adpcmencode   & 12.064   & 1700          &  \\
adpcmdecode   & 11.189   & 1700          &  \\
epic          & 46.858   & 1700          &  \\
unepic        & 35.302   & 47000         &  \\
jpegencode    & 11.807   & 47000         &  \\
jpegdecode    & 22.352   & 47000         &  \\
g721encode    & 10.232   & 170000        &  \\
g721decode    & 10.475   & 170000        &  \\
pegwitencrypt & 8.850    & 47000         &  \\
pegwitdecrypt & 8.788    & 47000         &  \\
\end{tabular}
\caption{Average Region Capacitance (FluidCap) vs Minimum Required Capacitance (BulkCap)}
\label{avg_cap}
\end{wrapfigure}


\noindent{\bf Preliminary Study: }
To highlight our approach over a standard 47μ bulk capacitor used in 
WISP5~\cite{wisp5}, we manually measured the capacitance required to finish each region partitioned by the region formation algorithm described in Task 1.
We compare the average capacitance over all regions (FluidCap) to the minimum capacitance requried to finish entire program (BulkCap).
As shown in 
2nd column of Table~\ref{avg_cap}, FluidCap can achieve much smaller average capacitor
size than by just using a bulk capacitor of a fixed size shown in the 3rd
column (BulkCap) of Table~\ref{avg_cap}. 
For g721encode, FluidCap achieves an average reduction in
capacitor size by 16614.5X and a minimum reduction of 36.28X.  
We believe that by using the small capacitor, 
FluidCap can achieve a much (more than likely order-of-magnitude) lower amount of total leakage energy.
This is substantial and lends credence that FluidCap
approach is promising.

\subsubsection{Task 2.2: Worst Case Energy Analysis of Idempotent Regions}

The biggest challenge to achieve the RockClimb idea is how to estimate the necessary
energy for each region. 
To address this challenge, we propose a new program analysis,
which we call worst-case energy (WCE) analysis, for conservatively estimating the
energy necessary to finish a region.  We believe that WCE can be realized on top
of existing worst-case execution time (WCET) tools. We plan to use abstract
interpretation based WCET analysis techniques~\cite{wcet2008} and modify it
to incorporate the processor power model.  
If the WCE analysis results are not accurate, i.e., overestimating the
necessary energy to finish a region, then we end up waiting longer than necessary before starting
the region after a power failure.
Thus, RockClimb's energy efficiency is affected by the accuracy of the WCE analysis.

Fortunately, building an accurate WCET tool and a precise power model will not be difficult for several reasons.
First, the simplicity of the processor used in the energy harvesting systems
would increase the accuracy of the model and the precision of the tool.  Second, 
 RockClimb only needs to estimate the energy of each region, not the
whole program, for the forward execution guarantee.
{\bf Since the WCET analysis is performed in a region-level, both
the scalability and the precision of the analysis will be greatly better than
that of whole program analysis counterpart}.  Third, in general, the code base of
embedded software running on energy harvesting systems is not that large, which is likely to make WCET
analysis free from the stability problem.

We plan to build a table of instruction energies by writing a
microbenchmark with a given instruction that was repeately executed and
measuring the average energy based on TI’s EnergyTrace software.
Using the same method, 
we plan to measure the NVM (e.g., TI MSP430FR's FRAM) accesses.
To calculate the WCE of a given region, we will first perform a topological
sorting of the basic blocks~\cite{muchnick97} and iterating through these
blocks. Given that the blocks are sorted this way, 
if we see a basic block then we know that the WCE of
its predecessor blocks has already been calculated. Thus, by calculating how much energy a given basic block
consumes, we can simply use a table to get the energy for each instruction and sum
the results. We will also keep a separate table of energies, where each entry is a
basic block and the total energy so far.  This is calculated by taking the
energy of a given basic block as described above and then adding the total
energy of the max energy of block’s predecessors. Therefore, when we have
calculated this, it is sure that the WCE of the region is the entry in the total
energy table for the final block in the region. After the WCE is taken we
will encode it in the region boundary instruction for controlling FluidCap's capacitance.

%\noindent{\bf Energy Efficiency of Rollback Free Mode}
\ignore{
There is another challenge that affects the energy efficiency of RockClimb's approach to guarantee the execution progress.
The configurable capacitor can affect the resulting energy-efficiency. For example, if the
capacitor stores only two levels of threshold energy and the energy estimated by the WCE
analysis is slightly over the first level, RockClimb might end up unnecessarily
waiting for the charged energy to reach the other
level. 
Here, we can formally represent such a delay by the time to charge each level of
energy during the WCE and the region formation analyses in our compiler.
Note that the recent research on
multi-power-source energy harvesting systems have already incorporated highly-configurable stacked (distributed) capacitors that support
fine-grained thresholds~\cite{teh2014,Hester2015,Zhang2015}.
}



\noindent {\bf Related Work: } 
To the best of our knowledge, no prior work guarantees the forward execution
progress.  Ratchet~\cite{rachet16} incorrectly argue that it can support the
forward progress guarantee with a watchdog timer that forces checkpointing at the
expiration. However, we formally proved that the timer based
approach leads to incorrect recovery when the idempotent region has
non-upward exposed anti-dependence~\cite{muchnick97} (e.g., store\&X;load\&X;store\&X) for which
Ratchet compier does not place a region boundary. Suppose that in the example, a
timer based checkpoint is made between the first store and the load, and power
faiilure occurs after the last store overwrites X. On recovery, Rachet starts the
execution from the location where the checkpoint is made, but it ends up loading
incorrect X which is overwritten by the last store. Thus, Ratchet fails to
recover from the failure. Recently, the same author proposes
Clank~\cite{hicks2017clank} as Ratchet's hardware
implementation for transparency. For the same reason, Clank's timer
based checkpoint leads to incorrect recovery. Consequently, neither Ratchet nor Clank
supports execution forward progress.
%\ignore{
Lucia et al. attempt to improve the progress with their profiling tool, that
computes the probability of power failures for a given static (i.e.,
manually-formed) tasks, to adjust the task boundary~\cite{cleancut16}. However, there is no guarantee that the actual
energy harvesting follows the profiled results due to its unpredictable/unstable nature.
All these prior works can suffer from stagnation due to the lack of forward
progress guarantee.
%}


