\subsection{Task 3: Stagnation-Free, Perforamnce-Oriented Region Formation and Adaptation}

\subsubsection{Task 3.1 Stagnation-Free Region Formation for Forward Execution Progress
Guarantee}
What if we cannot leverage a configurable capacitor such as FluidCap shown in
Task 2.1, i.e., only one level of energy threshold? Or what if some regions
require more energy than than the maximum energy, which can be charged by the
capacitor, in order to finish the region?  To answer the questions, we propose
to a new capacitor-aware idempotent region formation.  After the baseline region
formation used in Task 1 is performed in the first place, RockClimb compiler will look
for those regions whose necessary energy to finish the execution is greater than
the maximum charging capacity.  For those regions found, the compiler will place an
additional cut (i.e., idempotent region boundary) so that the execution of the
resulting partitioned region can be finished within the budget of the charging
capacity. Consequently, even in the presence of extremely frequent power
failures, we can always ensure forward execution progress by charging the capacitor
fully before the re-execution a failed region when power comes back.

\subsubsection{Task 3.2 Performance-Oriented Formation with Region Stitching}
\begin{figure}[t]
%\centering\includegraphics[scale=0.3]{figure/detection_latency_conf.pdf}
\includegraphics[width=\textwidth]{figure/region_combine.pdf}
\scriptsize
\vspace{-0.7cm}
\caption{Effect of region stitching}
\vspace{-0.4cm}
\label{fig:reg_com}
\end{figure}

Unlike Task 3.1, this task pursues performance-oriented region formation
assuming good energy harvesting quality with infrequent power failures.
We observe that the number of last-update (live-out) register checkpoints 
being inserted in each region is inversely proportional to its length, thus the longer region, the less
checkpoints.
In light of this, we propose {\it region stitching}, that combines multiple
idempotent regions to form a longer
region.
Another important observation is that 
the number of the checkpoints is proportional to the number of regions due to
more live registers to them.
Therefore, stitching the regions together can effectively reduce the number of regions,
resulting in the reduction of register checkpoints to be inserted. That way the region stitching
can siginificantly reduce the runttime overhead of failure-free execution. 

As an example, for two consecutive regions $Rg_1$, $Rg_2$ where a register $r1$ is updated
in the both regions and an input to $Rg_2$, 
stitching them together can eliminate the checkpoint for $r1$'s update in $Rg1$
because it is no longer the last-update (live-out) in the stitched region.
However, the region stitching is not free. There are two important
factors we need to carefully consider for stitching regions: (1) anti-dependent memory
accesses, and (2) the live-in registers.

First, anti-dependent memory access causes data inconsistency as with anti-dependent register access.
For example, suppose that in the beginning of some region, we load a value $N$, which is
initially 10, from the memory and increase
the $N$ by 1 and store it back 
to the same memory location, i.e., the value is now 11. Then, the system is immediately
crashed. For recovery, we re-execute the region for recovery thereby ending up loading $N+1$ (i.e., 11) instead of the
original value $N$ (i.e., 10). This is a problem of data-inconsistency, i.e.,
failure-induced data race~\cite{kolli2017language}.
This implies that the region stitching need to pay the cost for preserving the original value of
single memory location involved in the anti-dependence; recall that a region boundary is placed to
break such a memory-level anti-dependence.

Second, the number of live-in registers is another factor that must be carefully
considered for the region stitching. As a region becomes much longer after the
stitching, the number of live-in registers to the region might increase.  Thus,
this might lead to additional checkpoints for the registers in previous regions.
We plan to explore cost-benefit analysis models to determine what (how many)
regions need to be stitched. Our model will take into account three performance
factors: (1) the cost of preserving the memory value overwritten by the
anti-dependent store and inserting stitching-induced live-in registers if they
exist, (2) the benefit of removable checkpoints after stitching, and (3) the
impact that stitching can make on checkpoint pruning (Task 1), i.e., whether it
is improved (or supprressed) by stitching in a holistic manner.


\noindent{\bf Preliminary Results: }
%\noindent{\bf Preliminary Results}
As a feasibility study of our region stitching idea, we configured Gem5
simulator~\cite{gem5} to leave a trace of dynamically executed regions. By analyzing the
trace, we can figure out the decrease of register checkpoints to be inserted and the resulting
increase of live-in registers as the number of regions being stitched varies.
Figure~\ref{fig:reg_com} shows such a result.
Here, we stitch every 1 to 8 regions (x-axis of the figure)
to see the normalized checkpoint reduction ratio where 'no stitching' (i.e., 1 in the
x-axis) serves as our baseline.
As shown in Figure~\ref{fig:reg_com} (a), the more regions we
stitch, the less checkpoints to be inserted. This, in turn, indicates that the region
stitching leads to less performance overhead to be paid for
checkpointing registers.
It turns out that with the region stitching, we can reduce the necessary checkpoints more than 50\% (up to
80\%) for most of the applications.
Figure~\ref{fig:reg_com} (b) illustrates the average number of live-in registers
per a combined region across all the applications. 
The number of live-in registers we need to checkpoint is not necessarily
proportional to the number of regions being stitched. This implies that most of
the time {\it we can enjoy
the benefit of the region stitching without worrying about the side-effect, i.e., the
increase of live-in registers!}



\subsubsection{Task 3.3: Region Multiversioning: Stagnation-Free Region versus Performance-Oriented Region}
The proper length of regions depends on the frequency of power failures. As
mentioned in Task 2, if a region is too long, it would be stagnated when the energy
harvesting quality is poor. However, when the quality is good, i.e., power
failure is rare, stagnation-free short regions waste harvested energy for
unnecessary checkpoints in that most of the regions do not encounter power outage. 

On the other hand, long regions are beneficial since they can amortize the
checkpointing overhead. Also as shown in Task 3.2, stitched regions make it
possible to eliminate unnecessary checkpoints thanks to the reduction of
last-update (live-out) registers. Nevertheless, if the energy harvesting quality
is poor, i.e., power failure is frequent, such performance-oriented long regions waste
harvested energy due to the frequent long-distance rollbacks on failure.
Moreover, they have a much higher chance of stagnation than shorter
(non-stitched) regions.

With that it mind, we propose region multiversioning and the adaptation runtime.
The main idea is (1) to prepare multiple versions of each region at compile
time which differ in terms of the region length and (2) to have the adaptation runtime select the
right version at each region boundary based on dynamic feedbacks, e.g., power
failure frequency.

In addition to the original regions (version-1) generated by Task 1's algorithm that
cuts all memory anti-dependences, RockClimb compiler will generate three more versions;
(version-2) stagnation-free regions if the original regions require more energy to be
finished than the maximun charging capacity, (version-3) the same region formation but
without charging the capacitor fully thus trading-off the forward progress
guanrantee for saving the charging time,
and (version-4) stitched regions based on
the cost-benefit analysis techniques described in Task 3.2. 
In this way, RockClimb compiler will have four different types of region
formation (including the baseline region formation).

Among these four versions, the adaptation runtime will select the best version
taking into account power failure frequency to be monitored over time. We will
start with version-4 (i.e., stitched regions) with optimistic
speculation that there is now power failure. As power failure frequency gets
higher, the adaptation runtime gradually decreases the version
number towards version-1 (i.e., stagnation-free regions). Conversely, if the
frequency gets lower, the runtime selects a higher version towards version-1.

With this basic adaptation strategy, 
we will explore three different mechanisms to control the adaptation (i.e.,
version selection) in a reactive manner: (1) regression-based mechanism~\cite{jung05,adp-patent09,adp10},  (2) flux-based
mechanism~\cite{pande14,Sisalem98} inspired by TCP’s network congestion control
algorithm, (3) fuzzy-logic based mechanism~\cite{fuzzy10,Acampora2010}, and (4) queuing-model based
adaptation mechanism~\cite{InoueN14,welch64,Meisner2009}.
Along with power failure frequency, the adaptation logic will consult the other
dynamic feedbacks such as a type of energy harvesting source and its quality
factor.

\noindent {\bf Related Work: } 
To the best of our knowledge, there is no software based adaptation approach
that can maxmize the forward progress.  For hardware approaches, PI Jung's work
on nonvolatile processors mitigates the stagnation problem with two hardware
counters that control the gated store buffer and its
checkpointing~\cite{liuNVMSA16}.  Ma et
al.~\cite{ma2015dynamic,ma2017spendthrift} propose to throttle the execution mode
(e.g., out-of-order versus in-order pipeline executions) along with dynamic
frequency scaling.  For this purpose, the reconfigurable out-of-order
nonvolatile processor has a hardware neural network-based predictor.  Even
though the machine learning scheme can adapt to the varying conditions of energy
harvesting, it requires non-traditional expensive hardware supports which also
consumes harvested energy that could otherwise be used for execution progress.
