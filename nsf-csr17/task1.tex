\subsection{Task 1: Distributed Checkpointing for
Lightweight Power Failure Tolerance}
\label{sec:task1}
This task reformulates a problem of backup/recovery as that of preserving {\it
idempotence}, building upon PI Jung's prior work called Bolt~\cite{liuSC16} (SC'16
Best Paper Student Finalist: Highest-Ranked 1/446).  This task describes how
RockClimb extends Bolt for
power failure tolerance achieving correct and lightweight recovery.  The main
idea is that idempotent regions can be re-executed without changing their
output, thus we can recover from power failures occurred during a region
execution by restarting it in the wake of the system failure.

\noindent{\bf Lightwight Idempotence Based Recovery: } 
An idempotent region is a part of program code that can be freely re-executed to
generate the same output.  
More precisely, a region of code is idempotent if and only if its inputs are not
overwritten, i.e., no anti-dependence~\cite{muchnick97} on the inputs, within
the region.  Thus, the inputs to the entry of the region will
remain the same during the execution of the region, making it harmless to be
re-executed many times.  If some inputs are overwritten within the region, their
values do not remain the same as it were at the region entry.  Therefore, this
makes the re-execution of the region unsafe, i.e., ending up changing the
expected output produced by the region.  Consequently, it is a requirement for
the idempotence based recovery that the inputs to the regions should never be
overwritten within the region.


%~\footnote{ The stores in an
%idempotent region must be buffered as with branch misprediction handling in the
%commodity processor until they are verified, i.e., they are released when the
%region ends with no detected error.  }


To this end, Bolt~\cite{liuSC16} partitions entire program into preliminary regions with no memory
anti-dependences so that there is no memory input overwriting in the regions as in~\cite{dekruijf12}.
Therefore,
{\it preserving the register inputs becomes the only source of cost.}
This is a huge benefit over the prior work such as DINO~\cite{Lucia_2015},
Chain~\cite{colin2016chain}, and Alpaca~\cite{maeng2017alpaca}, because 
they version necessary memory inputs for each task 
boundary that needs to be manually reasoned and annotated by programmers.


Then, Bolt eagerly
checkpoints the value of {\it register inputs} to a region as soon as they are
defined. 
  Note, the compiler checkpoints once for each register input by
tracking the last update (i.e., only live-out register~\cite{muchnick97}); even if a register is defined multiple times in one
region, only one checkpoint is inserted right after the last definition. 
With such define-time live-out register checkpoints (we call them as {\it eager
checkpoints}), we can 
guarantee the recovery of all the inputs
to each idempotent code region even in the presence of power failures by
restoring the checkpoints before re-executing the failed region.

We call this technique {\it distributed checkpointing} unlike prior work
that checkpoints (or versions) registers in designated program points. For example,
Mementos~\cite{Ransford_2011} does at back-edges or function returns 
while DINO~\cite{Lucia_2015}, Chain~\cite{colin2016chain}, and Alpaca~\cite{maeng2017alpaca}
 do at manually-defined task boundaries; the state-of-the-art work called
 Ratchet~\cite{rachet16} does at every idempotent region boundary.
In contrast, we distribute their {\it centralized checkpointing} overhead to
multiple idempotent regions (referred to as regions hereafter) where
each register is defined. That is, RockClimb does not checkpoint entire register
file at every region boundary.


\noindent{\bf Checkpoint Pruning: } 
In particular, Bolt leverages a novel compiler analysis that can identify
unnecessary checkpoints in those eager checkpoints based on the following
insight.  {\it For a value corrupted or lost due to errors/failures, the original value can
be restored without checkpointing it, as long as it can be recomputed by
leveraging other checkpoints.} 
We formulate the problem of identifying the
checkpoints, that can be removed without compromising the recovery capability,
as that of finding a backward slice~\cite{Weiser81} which can recompute their original
value. We call such a slice as a recovery slice.  If it is successfully built,
our compiler removes the
corresponding checkpoint. On a power failure, our recovery runtime will simply
execute the slice to reconstruct the original value before jumping back to the
beginning of the failed region due to the failure.


\begin{figure*}[tbp]
				%\centering
				\includegraphics[width=\textwidth]{figure/motivate.pdf}
				\caption{Checkpoint pruning example with {\it byte\_reverse} code
				 of {\tt sha} in MiBench}
				 \label{fig:motivate}
\end{figure*}

Figure~\ref{fig:motivate} shows how our checkpoint pruning works as a
whole for the {\it byte\_reverse} function of {\tt sha} in
MiBench~\cite{guthaus01}.
(a) shows the code snippet of the {\it byte\_reverse} function.
(b) illustrates the control flow graph divided into idempotent regions ($Rg_0$,
$Rg_1$)
where dashed lines show the region boundaries.
(c) shows our eager checkpointing to provide guaranteed recovery where
the register inputs to $Rg_0$ are $R_0$$\sim$$R_7$.
(d) minimizes the performance overhead with checkpoint pruning where (e) is the
resultant recovery block for $Rg_0$.
For example, the value of $R_7$ can be reconstructed by
executing the recovery block that consults the checkpointed values of
$R_0$ and $R_2$.
In this example, we can achieve 
over 80\% performance improvement by pruning the
checkpoints in the loop without compromising the correct recovery guarantee.
We believe that once the checkpoint pruning optimization is implemented in our compiler, it will
significantly improve IoBT applications in that they are also
loop-dominant~\cite{wearcore16,eembc_iot16,PooveyG09} and the current
nonvolatile memory (NVM) incurs
higher energy/latency for writes~\cite{XueENM11}; checkpoints are essentially {\tt store}
instructions that write to NVM.




\noindent{\bf Why Distributed Checkpointing? } Before showing how to extend Bolt
for power failure tolerance, we point out three big problems of centralized
checkpointing (CC) of prior work to highlight Bolt's distributed checkpointing (DC).
(1) Hitting peak power: 
the CC executes many stores in a bulk manner at every task or region boundary.
According to the manual of MSP430FR microcontrollers~\cite{msp430manual}, a
series of consecutive stores hit the peak power consuming highest energy; the
authors of Ratchet~\cite{rachet16} report that checkpointing 8 registers requires
more than 40 cycles.  Thus, CC consumes considerable amount of energy at the
boundary, that would otherwise be used for making a better execution progress.
This implies that prior works with CC have much higher chance of power failures
at each task or region boundary!
(2) requiring bigger harvester/capacitor: for mitigating the high risk of
power failures due to the peak power hit, it is required to have a
bigger energy harvestor, e.g., larger solar panel, and a bulk capacitor whose
leakage current is
high~\cite{cherupalli2017determining,kontorinis2009reducing,cherupalli2017bespoke}.
Apart from the size issue of IoBT devices, CC based prior works are not so energy-efficienct.
(3) suppressing compiler optimization: in Ratchet~\cite{rachet16}, the CC forces registers to
be checkpointed at every region boundary (other works version them in every
task). This restriction prevents the
checkpoints, i.e., stores, from being moved.  For example, it is impossible to
get the checkpoints outside of a loop, which is an effective optimization to
reduce the overhead.  On the contrary, DC enables such an optimization thus
improving the performance significantly (Task 1.2).  Moveover, DC can achieve
more pruning of unnecessary checkpoints without compromising the recoverability
thanks to checkpoint coloring (Task 1.1).

\subsubsection{Task 1.1: Checkpoint Coloring to Avoid Overwriting the Storage}

\begin{wrapfigure}{R}{0.4\textwidth}
\vspace{-0.3cm}
\centering
\includegraphics[width=0.4\textwidth,angle=0]{figure/compensation.pdf}
\vspace{-0.6cm}
\caption{
\footnotesize
				\protect Checkpoint storage allocation %and symmetric instruction insertion
%(a) original program after 2-coloring.
%(b) symmetric instructions insertion.
%(c) optimization using path profiling information.
}
\vspace{-0.5cm}
\label{fig:MemAlloc} 
\end{wrapfigure}


Bolt assumes that the stores in an idempotent region must be buffered in the
gated store queue (GSQ) of processors until they are verified to be error-free,
i.e., they are released when the region ends with no power failure.  However, the
commodity microcontrollers used for energy harvesting systems do not have such
a GSQ. This implies that the memory location used for checkpoints can be overwritten by others, and therefore 
we must guarantee
that for each register, the previous checkpointed value in an earlier region is not overwritten by
the current region's checkpoint.
That is because the previous value might need to be restored as a live-in register of the
current region if it encounters a power failure~\footnote{GSQ obviates this,
as the value of current region's checkpoint held in GSQ is simply discarded on
power failure.}.
For such neighboring checkpoints, we attempt to allocate different storage
(i.e., designated memory locations for register checkpoint data). Interestingly, this can
be reduced to a 2-coloring problem.
Coloring the memory location for each checkpoint can be easily
achieved by performing a pre-order depth-first search during which different
memory location (i.e., each color) is determined. 
However, the coloring might be able to fail at a join point in the control flow
of program. 
To deal with the problem, we propose a simple control flow restructuring. 
Our compiler will selectively insert additional checkpoints to the problematic control flow
paths for making sure that all the colors from incoming paths share the same
color; this proposal calls such dummy checkpoint as symmetric instructions.
They will be inserted in the original program right after the conflicting region and form
a new region comprised of one or more symmetric instructions to different
variables that need to be checkpointed.

Figure \ref{fig:MemAlloc} shows the example; we color each checkpoint instruction
with either asterisk or pound sign in front of the checkpoint instructions
for each variable. Note that the region boundary is represented with the dashed
line in Figure \ref{fig:MemAlloc}.
First, we color the checkpoint instruction using a pre-order depth-first
search along one path. Then, we continue to color the other path.
However, a conflict happens when we try to color the other path as indicated
by the red arrow in Figure \ref{fig:MemAlloc} (a). Here, the two neighboring
checkpoints are assigned the same location (*) for a variable, Y.
To resolve the conflict, 
we create a new region between the conflicting regions and insert a symmetric
instruction. This is shown in Figure \ref{fig:MemAlloc} (b) where the conflict is resolved
since the inserted symmetric instruction can checkpoint Y to a different location (\#). 
That is, when a fault occurs during the execution of the bottom region, we can 
 restore Y by reloading the original value from the memory location (\#).
In general, the overhead of the inserted symmetric instructions would not be that
significant. However, there might be performance degradation if the
symmetric instructions are inserted to a hot path.
With that in mind, we will leverage path profiling so that it can
selectively insert the symmetric instructions on a cold path.
Figure \ref{fig:MemAlloc} (c) shows such an example of inserting
the symmetric instruction to the cold path.

We believe this checkpoint coloring technique can {\bf also improve 
checkpoint pruning}. The thing is that the main reason for preventing the
pruning is because the dependent checkpointed value is overwritten by the
neighboring checkpoint over control flow paths. This is exactly the problem the
checkpoint coloring will solve!  Currently, our checkpoint pruning can remove roughly 60\% of the register
checkpoints on average across SPEC2006/MediaBench/MiBench/SPLASH
benchmarks~\cite{liuSC16}. With the proposed checkpoint coloring, we hope to
remove more than 80\% of register checkpoints, rendering our distributed checkpointing nearly
overhead-free!

\subsubsection{Task 1.2: Checkpoint Scheduling}
\begin{wrapfigure}{L}{0.52\textwidth}
\centering
\includegraphics[width=0.52\textwidth]{figure/checkpoint_scheme.pdf} 
\scriptsize
\caption{The impact of checkpoint scheduling}
\label{fig:aaa}
\vspace{-0.3cm}
\end{wrapfigure}

Our distributed checkpointing makes it possible to schedule the checkpoints,
i.e., store instructions to checkpoint last-update (live-out) registers in each region.
Unlike centralized checkpointing, we will have a freedom to place checkpoint in an appropriate idempotent region.
Of course, we need to carefully place a checkpoint taking into account the
resulting performance overhead, i.e., this is essentially a checkpoint
scheduling
problem. To derive a optimal solution, we will evaluate the cost of each
candidate point for checkpoint insertion. 

Figure~\ref{fig:aaa} (a) and (b) show a checkpoint scheduling example from {\it libquantum}
in SPEC2006 benchmark suite. 
This program is divided into several idempotent regions indicated by the dashed lines.
Only the last region of basic block 3,
 has three live-in registers with anti-dependence, i.e., RBX, RBP and R13. 
In Figure~\ref{fig:aaa} (a), checkpoints are inserted in a lazy manner, i.e.,
they show up at the entry of a region; this is what
prior work does, e.g,  Ratchet~\cite{rachet16} checkpoints all live registers at the
beginning of each region.
Figure~\ref{fig:aaa} (b) demonstrates the advantage of eager checkpointing. Both
checkpoints for RBP and RBX are hoisted to BB1. R13 is checkpointed two times at BB2 and
BB3 in the eager manner that immediately checkpoints the last update.
Here, huge performance benefit can be achieved as the checkpoints for RBP and
RBX are no longer inside the loop. 
Note that we have also found many other examples in which lazy checkpointing beats the eager one for the similar reason, i.e., the reduced number of checkpoints.
Figure~\ref{fig:aaa} (c) indicates that the hybrid of the lazy and eager schemes referred to as
"optimal checkpointing" can further reduce the number of checkpoints being inserted.
Here, one checkpoint for R13 in BB2 is removed in this optimal manner.

To realize this idea, we propose static and dynamic checkpoint cost evaluation.
we will first perform postpass optimization,
that analyzes definition-use relation for those registers with anti-dependence,
in order to estimate the static cost of each checkpoint candidate; this will be implemented using traditional compiler techniques such as {\it use-def} chain construction~\cite{muchnick97}. 
Then, dynamic profiling information will be leveraged to increase the precision of the
statically estimated cost.




\noindent{\bf Benefits over Related Work: } 
Table~\ref{tb:comp} gives RockClimb's high level comparison to prior works.
DINO, Chain, and Alpaca schemes require considerable user efforts.
Programmers should deal with unfamiliar language features such as
''transition\_to'' or ''task\_boundary'' and data type (e.g., TS), to use the
schemes. Even worse, users have to reason about right task boundary or
function declaration in those schemes, which is based on impractical assumption that 
users are aware of memory inconsistency problem and
safe amount of energy consumption for each function. 
In other words, users have a responsibility of both incorrect recovery and
stagnation on frequent power failures. In contrast,
RockClimb proposes an automated compiler-directed checkpointing/recovery approach
as in Ratchet, so that users can be freed from the dauting reasoning. 

\begin{table}[htbp]
				\centering
				\scriptsize
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
 & Recovery & Checkpointing  & Progress & Failure & Energy & Checkpoint & Checkpoint & Physical \\ 
				& boundary & (versioning)   & guarantee & -free  & consumption & prunning & scheduling & checkpointing \\ 
				& reasoning & type  & (Task 2/3) & overhead & at boundary  &
				optimizating & (Task 1.2)  & (Task 4) \\ \hline
DINO & Manual & Centralized & no & 150${\sim} 400\%$ & High & no & no & no \\ \hline
Mementos & Auto & Centralized & no & 90${\sim}200\%$ & High & no & no & no \\ \hline
Chain & Manual & Centralized & no & 90${\sim}200\%$ & Medium & no & no & no \\ \hline
Alpaca & Manual & Centralized & no & 90${\sim}200\%$ & Medium & no & no & no \\ \hline
Ratchet & Auto & Centralized & no & ${\sim}10$${\sim}15\%$ & High & no & no & no \\ \hline
				\bf{RockClimb} & \bf{Auto} & \bf{Distributed} & \bf{yes} &
				${\sim}$~\bf{5\%} & \bf{Low} & \bf{yes} & \bf{yes} & \bf{yes} \\ \hline
\end{tabular}
\caption{Comparison to related work}
\label{tb:comp}
\end{table}


Unlike the prior works,
RockClimb can reduce the number of
checkpoints without compromsing the recoverability and leverage more energy for
forward progress, using checkpoint pruning and scheduling (Task 1.2)
optimizations. Note, it is the RockClimb's distributed checkpointing (DC) that enables these
optimizations. Again, thanks to DC, RockClimb do not consume high energy at 
each region boundary. For example, Ratchet checkpoints all live
registers at region boundary, and it loads the entire register file from
nonvolatile memory (NVM) at
recovery time~\cite{rachet16}. Chain and Alpaca consume less energy at their
task boundary, but to a large extent, this results from user's well partitioned
task boundary.

 
Moreover, RockClimb is the first that solves 
the problem of forward progress guarantee, the long-waited open problems in the
energy harvesting system area.
Finally, for the first time, RockClimb (Task 4) proposes novel techniques to
deal with power failures during sensing operations.


\noindent{\bf Preliminary Study: }
To gauge the impact of our approach, we  
tested the benchmarks used in DINO and Menento. The
benchmark binaries were generated by using PI Jung's Bolt~\cite{liuSC16} compiler without checkpoint
coloring (Task 1.1) and scheduling (Task 1.2). We ran all Bolt-compiled binaries on 
TI's MSP430FR5969 microcontroller using its FRAM as main memory not to use volatile SRAM.
Compared to the prior schemes whose overhead was quoted from their papers,
RockClimb's performance overhead is quite negligible, i.e., less than 5\%
as shown in Table~\ref{tb:comp}.
We believe that if checkpoint coloring and scheduling optimizations are
implemented, the performance overhead will decrease. That is because the coloring
enables more checkpoint pruning as mentioned in Task 1.1, and the 
scheduling (Task 1.2) can take away unnecessary checkpoints further.


\ignore{
\begin{wrapfigure}{R}{0.36\textwidth}
\centering
\scriptsize
  \begin{tabular}{|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|@{}c@{}|}
%\begin{tabular}{|c|c|c|c|c|}
\hline
   				&  Auto-  & Fault-free  & How to 	& Progress \\ 
   				&  matic  & Checkpoint  & Checkpoint	& Guarantee\\ \hline
 Mementos &  Yes    & $>$ 100\%   & Centralized		     & No \\ \hline
 DINO     &  No     & $>$	100\%   &	Centralized   	   & No \\ \hline
 \bf RockClimb &  \bf Yes   & \bf $<$ 10\%	  & \bf Distributed 		   & \bf Yes (Task 2) \\ \hline

\hline
\end{tabular}
\scriptsize
\vspace{-0.2cm}
\caption{Comparison to prior work}
\label{tb:comp}
%\end{figure}
\end{wrapfigure}


\noindent{Benefit over Prior Work: } 

\begin{wrapfigure}{R}{0.5\textwidth}
\includegraphics[width=0.5\textwidth,angle=0]{figure/rockclimb_overhead.pdf}
\centering
\small
  \caption{Runtime overhead of RockClimb (3rd bar)}
  \label{fig:data1}
  \vspace{-0.2cm}
\end{wrapfigure}


For the first time, RockClimb will leverage idempotent
processing for power failure tolerance. 
Figure~\ref{tb:comp} summarizes its benefits over prior work such as
Mementos~\cite{Ransford_2011} and DINO~\cite{Lucia_2015}. 
Any power failure tolerance schemes should be lightweight to avoid aggravating the 
shortage of the energy harvested from unreliable ambient sources for IoBT devices.

To justify the RockClimb approach, we have already started implementing basic features of RockClimb's compiler. It currently  
inserts a checkpoint right after the register is defined last time in each
region, ensuring that the previous checkpointed value in an earlier region is not overwritten by
the later region's checkpoint by versioning the checkpoint storage.
For a set of benchmarks used in the prior work~\cite{Lucia_2015,Ransford_2011},
we compares RockClimb to Mementos on top of real board with 
TI's MSP430FR5969 microcontroller in terms of the execution time overhead when
there is no power failure; DINO was excluded since it requires manual task
boundary annotation. Here, we used the FRAM of the microcontroller
as main memory not to use volatile SRAM. 

Figure~\ref{fig:data1} shows the execution time overhead of RockClimb (3rd bar) and
two versions of Mementos over the baseline
that has no power failure tolerance in a {\it logarithmic} scale.
On average, RockClimb incurs less than 10\% overhead whereas Mementos does
way more than 100\% overhead.
Unlike DINO, Mementos does not preserve
memory inputs with anti-dependence for correct recovery, i.e., Mementos'
overhead will increase with the care.
Note, RockClimb's overhead will be dramatically reduced by leveraging the optimizations we
propose below in Task 1.2.
The takeaway is that RockClimb offers 
lightweight power-failure-free execution with {\bf order-of-magnitude smaller
overhead} than the prior work.

}

\noindent{\bf Metrics for Success: } The Task 1 will be evaluated in terms of the
execution time and the energy efficiency on top of (1)
architecture simulator with power traces collected by real energy harvesting and
(2) the real machine board with various actual energy harvesters.
For this purpose, we will leverage the resources available in
Center for Energy Harvesting Materials and Systems (CEHMS)~\cite{chems16} at
Virginia Tech.
%, a part of the NSF Industry University Collaborative Research Center.
