
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sniper Overview}
\label{sec:sniper}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The goal is to automatically detect memory leaks in C/C++ applications serviced on bare-metal clouds.
Sniper will leverage instruction sampling using performance monitoring units (PMU) in processors
 to track accesses to heap objects without significant overhead.  
It will also offload most of time- and space-consuming work, e.g., tracking heap organization and searching for the heap object accessed by a sampled instruction. 
To achieve this, we propose a trace-driven approach based on the combination of a lightweight heap trace generation and an offline trace simulation.  

\begin{figure}[t!]
\centering
\begin{minipage}[b]{0.65\columnwidth}
%\begin{minipage}[b]{\figurewidth\columnwidth}
\centerline{\includegraphics[width=\columnwidth,angle=0]{figures/framework.pdf}}

\end{minipage}
\centering
\textsf{
	\caption{\protect The Sniper Organization.}
	\label{fig:framework}
}
\end{figure}

Figure~\ref{fig:framework} shows a high-level view of Sniper.
First, an application binary is fed into Sniper's launcher.
It prepares a {\tt ptrace} hook so that a {\tt ptrace} monitor 
observes every PMU transaction from the core the application is running on.
That way Sniper can collect the instruction samples without perturbing the
application execution.
Then, the launcher preloads Sniper's wrapper (.so) to hijack heap interfaces, e.g., malloc/free, and fork and executes the application.
At runtime, the wrapper generates traces of the functions to track how the heap
organization evolves. They are buffered and later recorded in files.
PMU traces are recorded in a similar manner.

Once the application completes execution, all the traces are fed into a trace simulator. 
To extract program context information, it consults the binary
analyzer. During the simulation, it tracks the interaction between the application and the heap organization as well
as the accesses to the heap-allocated objects. 
I.e., the simulator replays the application's execution in terms of its
heap usage, and updates the {\it staleness} of the allocated objects.  

At the end of the simulation, Sniper finally reports leaks detected with its
anomaly detection to Bugzilla.  In particular, Sniper's report is rich with details about
each object including the program context (malloc/free
sites\footnote{Sniper will record the return address of malloc and free at runtime.
Later the offline trace simulator will calculate the their actual site address from the return
address by analyzing the application binary.}) and various simulation results
such as memory access/growth analysis.  While prior work reports just the last access
site of only leaking objects, i.e., a single instruction address, Sniper will provide an
snapshot of different instruction accesses to the object whether or not it is a leak
thus helping developers to fix it while debugging.









%With that in mind, Sniper leverages a statistical analysis on the trace
%information as well as detailed results of the trace simulation.
%In particular, this work reformulates the problem of memory leak detection as
%that of anomaly detection. Thus Sniper views memory leaks as anomalies.
%The reason for this is that the {\it staleness} of a leaking object should be extremely higher than
%that of considerably many normal objects in the entire application or even in
%the same allocation site.
%The end result is that Sniper can automatically determine the {\it staleness}
%threshold in an application-specific manner.


%Second, to help debugging of leaks, Sniper tries to find the program flow from
%the allocation of objects to the point of last use. 
%For both leaking and normal objects, it constructs a dynamic slice to analyze
%how they differently behave, which can be the cause of leaks.


%The purpose of Sniper is to provide a tool that can correctly detect memory
%leaks in applications without incurring a significant overhead to make it usable
%for production software. 
%To achieve this, Sniper leverages instruction sampling
%using performance monitoring units (PMU) of underlying processors.  Such a
%fine-grained sampling is robust against false negatives in that access the
%corresponding staleness is updated relatively quickly.  Sniper is also robust
%against false positives, since the sampling is not biased for any program path,
%i.e., stalenesses are updated evenly in a path-agnostic way.  To further reduce
%the runtime overhead, Sniper exploits multi-threaded execution to offload the
%additional work for the memory leak detection from the application thread.  With
%the help of the PMU profiling and the carefully designed multi-threaded execution,
%the runtime overhead of Sniper is 2\% on average.


%Sniper updates the staleness of the corresponding
%object, if they belong to one of heap-allocated objects.  To determine this,
%Sniper leverages a succinct data structure called {\it single interval
%tree}.  Note that multi-threaded execution offloads every operation, i.e., {\it
%insert/delete/search} on the data structure from the application thread using lock-free queues.
\subsection{Task1: Trace-Based Lightweight Memory Monitoring}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Memory Access Tracking with PMU-Based Instruction Sampling} 
\label{sec:pmu}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The key to detect leaks is the {\it staleness} of allocated objects in that if
they have not been accessed for a long time, they are likely to be leaks.  
By its definition, i.e., the elapsed time from the last access, 
the {\it staleness} calculation requires tracking the memory access to the objects.
For efficient leak detection, it is important to collect the last access with a
low overhead. With that in mind, we will obtain the memory access profile through the
PMU hardware support 
 called PEBS~\cite{nhmPMUGuide}
available on commodity processors.

That way, we will be able to track memory accesses without incurring a significant overhead. 

%An instruction sampling is a hardware mechanism that offers a good insight into program
%behaviors with a very low overhead.
%The PMU on modern processors has a special mode called event-based
%sampling~\cite{nhmPMUGuide,amd07}. 
%For a given event, this mode can configure the corresponding performance counter to
%raise an interrupt on overflow of its value, i.e., sampling period; 
%when an interrupt occurs, the instruction that
%causes the overflow can be queried.  
%As an extension to that, Intel's PEBS (Precise Event Based Sampling) \cite{nhmPMUGuide} not only provides
%precise location of the event in the instruction space, but also provides a way
%to access the register contents of the instruction that causes the event.
%Likewise, AMD's IBS (Instruction Based Sampling) supports reading the
%virtual address in the target register of retired {\tt load/store} instruction 
%together with its address.
%Similar sampling support is available on other microarchitectures such as Intel
%Pentium4/Itanium, IBM POWER5, Sun UltraSparc, etc.

We will sample memory accesses, i.e., {\tt load(store)} events, to
capture both the instruction and data addresses in the target register through the PMU based instruction
sampling.
Such information about each sample along with its timestamp will be recorded in
the trace files.  Later, the trace simulator will determine whether sampled instructions
access a heap object. That is, if there exists a heap object that embraces the
data address of the instruction when it executes, the simulator will update the {\it staleness} of
the object based on its timestamp.



%Currently, Sniper supports process- and thread-aware sampling with the help of
%Perfmon2 kernel interface~\cite{perfmon2} and ptrace system calls.  Sniper
%intercepts process/thread creation requests through the ptrace hook, and
%creates a PMU context for each thread; the context contains appropriate PMU
%configurations including event types and sampling periods.  Sniper then attaches
%the PMU context to the corresponding thread to be created.
%
%On a context switch, the kernel reconfigures the PMU according to the attached PMU
%context, and enables the sampling of memory accesses. With this support,  Sniper
%can monitor and save thread-level information thereby effectively dealing with
%multithreaded applications.
%During program execution, Sniper monitors the application's PMU transactions, i.e., memory access
%samples, and fetches the samples through the Perfmon2 kernel interface.  

It is important to
note that Sniper will not interrupt the target application execution at all.
Thus, Sniper can keep track of the heap accesses without perturbing the original
application execution. In particular, to prevent a majority of samples from falling into a synchronized pattern in some loops,
Sniper will leverage the sampling period randomization, i.e., adding a small
randomized factor to the period.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Lightweight Heap Trace Generation} \label{sec:heaptrace}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
To track the {\it staleness} of the heap objects, we should be aware of the
 heap organization in terms of its allocation and deallocation during program execution. 
For this purpose, we should record full traces of malloc/free and the
related program context information.
%Even if the trace simulator can take over much of the heavy work such as 
%tracking heap organization, Sniper still
%needs to minimize the overhead of the trace generation.

Predictably, the trace is likely to occupy a large amount of memory space to store the tag data of each
heap object which includes its allocation/deallocation/last-access sites, heap
organization information, e.g., the range information of an allocated object and
freed address, etc\footnote{The space for the {\it staleness} is not necessary
at runtime because it is calculated offline by the trace simulator.}.
To tackle the space overhead, Sniper will buffer the tag data trace and flush the
buffer into a file when it is full.
In this way, the memory consumption of the trace generation can be bound to the buffer
size.

Especially for a multithreaded application, Sniper should take care of contention
to the buffer and the file from multiple threads. Of course, Sniper should
guarantee that multiple threads write their trace correctly. One way to do that
is relying on locking mechanism on the buffer and the file. However, this causes
unacceptable performance degradation of the application due to the high
synchronization overhead as the number of threads increases.
Instead,  we plan to allocate both a buffer and a file into each
thread, thus they become
thread-private. This makes the buffer accesses lock-free and allows Sniper to use 
{\tt fwrite\_unlocked} for lockless file writing. 

{\it The challenge here is that the trace simulator must have a
synchronized view of traces from multiple threads}.
Note that Sniper can achieve this with no additional cost, since it associates
each of malloc/free/PMU traces with its timestamp in the first place for
 single-threaded applications. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Offline Trace Simulation} \label{sec:simulator}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Sniper will offload the time- and space- consuming work of the {\it staleness}
update for heap objects, which requires tracking heap organization and searching for the heap object
accessed by a sampled instruction. To achieve this, we will leverage the
lightweight heap trace generation and its offline
trace simulator that takes over the heavy work.
In particular, the simulator will build the tag directory based on recorded traces 
and performs expensive tag searches to calculate the {\it staleness} offline.

Once the traces of malloc/free/PMU(memory access) are recorded at the end
of program execution, all the trace files are fed into the simulator.
An application binary is also fed into the simulator for its binary analyzer to extract an actual
allocation site address, i.e., the instruction address of {\tt call} to malloc/new, based on
their return address in the stack trace; the same thing is with deallocation and last-access sites.

\begin{figure}
\centering
%\begin{minipage}[b]{\figurewidth\columnwidth}
\centerline{\includegraphics[width=0.65\columnwidth,angle=0]{figures/sim.pdf}}
%\end{minipage}
\caption{\protect
The time-synchronized trace simulation\label{fig:sim}}
\end{figure}

Then, the simulator first merges the traces in the files and sorts them by the timestamp of each
trace, which gives the simulator a time-synchronized view of all the traces,
using MapReduce~\cite{dean04}.
While the simulator is running, it will decode each trace in turn and perform appropriate
actions according to the decoded results.
Figure~\ref{fig:sim} shows this process.
%and Table~\ref{tb:rule} summarizes the different actions the simulator takes.
%%
%\begin{table}[hbt]
%\footnotesize
%\renewcommand{\baselinestretch}{1.0}
%\centering
%\begin{tabular}{|l|l|}
%\hline
%Trace Type & Simulator Action (Necessary Tag Directory Operation) \\
%\hline
%\hline
%malloc & Create a new tag of the allocated object \\
%       & (insert the tag into the tag directory) \\
%\hline
%free & Remove the tag of the freed object \\ 
%     & (search for the tag and {\tt delete} it from the tag directory) \\
%\hline
%memory  & Update the {\it staleness} of the accessed object \\
%access  & (search for the tag for the object and reset its {\it staleness}) \\
%\hline
%\end{tabular}
%\caption{
%	\footnotesize
%	Simulator action according to the type of traces. Tags specify the
%address range of an heap object, i.e., the start address and the size, as well
%as other metadata, e.g., {\it staleness}, timestamp, malloc/free sites, and
%so on.\label{tb:rule}}
%\end{table}



To keep track of heap organization, we plan to model each heap object with the start and end addresses of the object in the
tag, and to maintain a tag directory to manage the tags.  When the simulator
processes a malloc (free) trace, the corresponding tag will be created/inserted
(removed) in the directory.   For a memory access trace,
the simulator will determine if the address of the access corresponds to one of heap objects. This involves a search for the corresponding tag whose range
(the start of the tagged object $\mathtt{\sim}$ its end address) embraces the
queried address in the tag directory.
 If the search succeeds, i.e., a heap object access,
 the {\it staleness} of the resulting tag for the object will be calculated and
 recorded by the simulator.
 Since it tracks the heap organization, each access is
 correctly attributed to the corresponding heap object.

In summary, the simulator will replay the program execution in terms of
heap usages, updating the {\it staleness} of the allocated objects.
That way Sniper can catch every leak occurred during the execution
that generated those traces being simulated.


\begin{figure}
\centering
\begin{minipage}[b]{0.9\columnwidth}
%\centerline{\includegraphics[width=0.5\columnwidth,angle=0]{figures/tree.eps}}
\centerline{\includegraphics[width=\columnwidth,angle=0]{figures/gfs.pdf}}
\end{minipage}
\caption{\protect
The bare-metal cloud datacenter environment\label{fig:center}}
\end{figure}

\paragraph{Optimization for the Trace Simulation}
Since Sniper keeps generating traces at runtime, the size of the trace files can
quickly grow; this is the common concern of many trace-based profilers.  
We believe parallelization and selective analysis can reduce the trace size and the simulation time.
One possible optimization to reduce the trace size (simulation time) is
periodically processing partial trace files during
program execution. 
Once simulation outputs are generated, most of the trace files can be deleted. Those malloc
traces having the corresponding free traces can be deleted too.
However, for incremental {\it staleness} update, any information necessary to
track the heap organization should be maintained.
For efficiency, the partial trace files can be transmitted to other available
machines in a
pipelined way for the remote simulation. 

For this purpose, we plan to have dedicated analysis machines in the bare-metal
cloud. In reality, they often exist in commercial datacenters to process the log and trace data of production machines, which is
true for Google's datacenter~\cite{ren10}.
Sniper can thus leverage such machines to enable the remote simulation.
Figure~\ref{fig:center} shows the cloud datacenter environment.
In particular, both production and analysis machines share a distributed file system, e.g.,
Google File System~\cite{gfs03}, which is connected to a separate a gigabit network. 
Thus, writing a trace file rarely affects the QoS of the application which is serviced using another
network, e.g., Internet; the production machines are equipped with two NICs for each separated network in order to
keep the overhead minimal.





\subsection{Task2: Automated and Systematic Memory Leak Determination}
\subsubsection{Systematic and Automated Leak Identification Using Anomaly Detection}
\label{sec:anomaly}
Once the trace simulation finishes, Sniper is ready to report leaking objects based on
the {\it staleness}.  An important issue is how to determine the
$threshold_{staleness}$.  It is very important to precisely determine the
threshold, 
since it directly impacts the number of false positives and negatives.

Indeed, it is difficult,
costly, and error-prone for users to set the threshold correctly. 
Upon any change, such a high cost of threshold determination will have to be paid anew.
In particular, the threshold should be different across applications, and 
there is no one-size-fit-all solution even in the same application.
%Thus, multiple thresholds should be carefully determined according to
% application characteristics. 
{\it Thus, the challenge is that leak identification 
should not only be a systematic and automated, but also be tailored for each
 application.}

With that in mind, we will leverage a statistical anomaly detection, i.e.,
Sniper will view leaks as anomalies.
That is based on a couple of observations; (1) 
the {\it staleness} of a leaking object is very high compared to normal
objects allocated in the same site, which is the basic philosophy of {\it
staleness}-based approaches.
(2) the number of leaks is a lot smaller than that of normal objects,
which is true because production software should undergo a number of extensive testing
procedures from its creation to the release. In fact, large software companies
such as Google has already adopted the test-driven application
development~\cite{tdd08}. Without passing various test cases, developers
cannot submit even a single line of change to code repository. That way naive leaks in
applications are likely to be detected before its production release.

\paragraph{Anomaly Detection with Adjusted Boxplots}
We transform the problem of leak detection into that of anomaly
detection for univariate data set which is comprised of the {\it staleness} of
objects.
The issue here is that most of anomaly detection techniques assumes underlying
distribution, e.g., {\it boxplot} approach works best for normal distribution as
other approaches favor it~\cite{wilcox2012}.

{\it The challenge is that the leak detection problem does not follow normal
distribution.} That is because leaks are not relatively many whereas normal objects are dominant, 
and the {\it stalenesses} of leaking objects is very large compared to
that of normal objects.
Thus, the distribution of {\it stalenesses} data tends to be right-skewed, i.e. having a
long tail in the positive direction, which can paralyze the anomaly detection
capability of a naive approach. 

In light of this, we will leverage a different approach called {\it adjusted
boxplots}~\cite{wilcox2012} for the anomaly detection.
In contrast to the original {\it boxplot} that classifies all points outside the
 interval of $\left[Q_1 - 1.5 \mbox{ }IQR ; Q_3 + 1.5 \mbox{ }IQR\right]$, where
$Q_1$ and $Q_3$ are 1st and 3rd quartiles respectively and $IQR$ is $Q_3 - Q_1$,
as \textit{potential} anomalies, the {\it adjusted boxplot}
shifts the interval with the consideration of how the underlying
distribution of the data set is skewed.
For the systematic leak determination, Sniper will set the $threshold_{staleness}$ as the upper bound of the {\it
adjusted boxplot} defined as;
%The boxplot is a frequently used graphical techniques for analyzing a univariate
%data set. It splits the data set into quartiles and displays a line at the
%height of the sample median, a box from the first quartile to the third quartile
%whose length equals the interquartile range $IQR = Q_3 - Q_1$. It also
%classifies all points outside the interval $\left[Q_1 - 1.5 \mbox{ }IQR ; Q_3 +
%1.5 \mbox{ }IQR\right]$ as \textit{potential} anomalys.
%

\[
\footnotesize
Threshold = \left\{
\begin{array}{c l}
Q_3 + 3.0 e^{3 \mbox{ } MC} \mbox{ } IQR & \quad MC \geq 0 \\
Q_3 + 3.0 e^{4 \mbox{ } MC} \mbox{ } IQR & \quad MC < 0
\end{array}
\right.
\]
%In particular, Sniper exploits a different IQR coefficient (3.0), which is called {\it extreme anomaly} detection in the literature. 
%This has an effect of identifying leaks more conservatively, i.e., for less false positive.
where {\it medcouple} $(MC)$, i.e., a {\it robust} measure of the
skewness of underlying distribution, is defined as;
\[
\footnotesize
MC = \operatorname*{med}_{x_i \leq Q_2 \leq x_j} {h(x_i,x_j)}
\]
where $Q_2$ is the sample median and for all $x_i \neq x_j$. That is, {\it MC}
is the median of the kernel function ($h$) results where $h$ is given by;
\[
\footnotesize
h(x_i,x_j) = \frac{(x_j - Q_2) - (Q_2 - x_i)}{x_j -x_i}
\]

%For the special case $x_i = Q_2 = x_j$ a specific definition applies, see Brys el al. (2004) for details. The medcouple
%equals the median of all $h(x_i,x_j)$ values for which ${x_i \leq Q_2 \leq x_j}$.
%
%$MC$ is inspired by the quartile skewness and always lies between $-1$ and $1$.
%A distribution that is skewed to the right has a positive value for the
%medcouple, whereas the $MC$ becomes negative at a left skewed distribution.
%
%Thus, the above formula for the threshold computes highest datum still in $1.5
%IQR$ of the upper quartile adjusted by the term $e^{3 \mbox{ } MC or 4 \mbox{ }
%MC}$ for skeweness of the underlying distribution of data.


\subsubsection{The Granularity of the Anomaly Detection}
\label{sec:anomaly}
Depending on the scope to which the statistics is applied, the resulting
accuracy of memory leak determination might be improved.
We plan to have three different schemes based on the scope of the statistical
anomaly detection, e.g., an entire application/allocation site.

\paragraph{Local Detection} Sniper can apply the anomaly detection for each
allocation site (or calling context beyond the site), which is called local detection.
This scheme has potential to achieve higher accuracy, since it performs
allocation-site-specific (or fully-context-sensitive) leak detection.
Even when the inequality~\ref{eq:acc_cond} does not hold for entire objects, the
scheme can still detect leaks with no false positive/negative.
I.e., by narrowing down the scope of leak detection to those objects
created in the same site, the inequality~\ref{eq:acc_cond} is likely to hold.

However, the local scheme can be misleading depending on the
state of an allocation site. They might occur for a couple of reasons; (1) insufficient amount of
sample data; if some site has a few objects,
e.g., $<$ 10 objects, in which case no statistical method works.
(2) similarity of sample data; even with the abundant amount of sample data, {\it stalenesses} of the objects
could have not much difference, in which case even humans cannot detect any anomaly.
E.g, it would be the case where every object created in one site is all
leaking, or no object is leaking.

\paragraph{Global Detection} To deal with the problems, Sniper can perform the anomaly
detection for entire objects within the application, which is called global detection. 
Note that the global scheme still performs the application-tailored leak detection but not in the allocation-site-specific way.
When the local scheme fails to detect leaks due to its high threshold
($local\_threshold_{staleness}$), the
global scheme would be a good alternative. E.g., when those objects created in
the same site are all leaking, the global scheme can still detect them in that its threshold ($global\_threshold_{staleness}$) is likely to be
smaller than their stalenesses.

Then the question is how to make a correct decision to pick the right
detection scheme for each case. 
By doing that, Sniper can take advantage of the synergy between the local/global schemes thereby achieving higher accuracy.
On the contrary, an incorrect decision translates to false positives/negatives.
With that in mind, this work designs Sniper's hybrid detection scheme.

\paragraph{Hybrid Detection} We plan to perform the local detection for each site in the first place.
The idea is that Sniper respects the leak report of the allocation-site-specific detection scheme.
%That is, 
For only those allocation sites that report no anomaly (leak), does Sniper consult the global detection scheme. 
For the local and global schemes to generate a different result, the
staleness spectrum of the objects in the site has to be overlapped with the
interval of the two thresholds of the both schemes.
I.e., the candidate sites for the hybrid detection is defined as 
\begin{equation}
\footnotesize
\begin{split}
candidate\_sites = \{ site | site \in \mathcal{S}, 
global\_threshold_{staleness} <  \\
\max_{o \in site} staleness(o) < local\_threshold_{staleness}(site)\} 
\end{split}
\end{equation}
where $\mathcal{S}$ is a set of allocation sites in an application.

For each candidate site, the hybrid scheme will simply use the global scheme assuming that the site's objects are leaking.
The intuition behind the heuristic is two-fold; 
(1) it follows the philosophy of the original {\it staleness}-based leak
detection~\cite{chilimbi04}, i.e., highly-stale objects are likely leaking. 
(2) Sniper must not miss leaks. Otherwise, it loses its worth as a leak
detection tool.

However, the heuristic might end up with false positives in case the assumption
is wrong. Note that this is rather a limitation of {\it
staleness}-based leak detection, i.e., it is possible to incorrectly blame the
objects that are highly {\it stale} but that do not actually leak.
As an example, even if GUI objects might not be accessed for a long time after
their creation, they should not be reported as leaks~\cite{xu08}.
{\it This is a challenge to be overcome}. Our strategy to deal with it is based on
users' expectation for a leak detection tool.


In general, users are interested in the critical leak that
impacts overall memory consumption. That is, they would not care
about a leak which rarely affects memory consumption, even though it is
highly {\it stale}.
With that in mind, Sniper selectively applies the heuristic according to how much
stale objects contribute to total memory consumption, i.e., the hybrid scheme switches to the
global one only for the following sites;
\begin{equation}
\footnotesize
\begin{split}
hybrid\_sites = \{ site | site \in candidate\_sites,  \\
\sum_{obj \in site} size(obj) \,\,\,\,\,\,\,\,\, > \,\,\,\,\,\,\,\,\,\theta \sum_{obj \in allocated\_set} size(obj) \} 
\end{split}
\end{equation}
where {\it allocated\_set} is a set of the objects that have been allocated but
not yet freed at time $t_{report}$.
That is, if it turns out that the stale objects detected by the global scheme do not contribute that much, 
the hybrid scheme will remain at the local scheme.
Note that $\theta$ is a configurable parameter which takes into account the application's SLA
and the QoS requirement. 


%Algorithm~\ref{algo:siteset} shows how the {\it stale\_site\_set} is constructed in
%pseudo code.
%
%\begin{algorithm}
%\caption{{\it Decision making process of Sniper's hybrid leak detection}
%\label{algo:siteset}
%\begin{algorithmic}
%\REQUIRE $not\_freed\_set$ 
%$ sites\_without\_anomaly = \{ site | site \in program, \max_{o \in s} staleness(o) < local\_threshold_{staleness}(site) \} $
%\FORALL{$site \in sites\_without\_anomaly$}
%
%\IF{$global\_threshold_{staleness} < local\_threshold_{staleness}(site)$}
%	\IF{$\sum_{obj \in site} size(obj) < \sum_{obj \in not\_freed\_set} size(obj)$}
%		\STATE switch to a global scheme
%	\ELSE
%		\STATE stay at a local scheme
%	\ENDIF
%\ENDIF
%
%\ENDFOR
%
%\end{algorithmic}
%\end{algorithm}

%Once candidate objects are first selected by $threshold_{stalenss}$ based on the
%anomaly detection, 
%Sniper calculates the {\it MGC} of the objects to evaluate
%their contribution to the total memory consumption.




%\subsection{Fallacy of False Positives and Negatives}
%All the {\it staleness}-based tools have evaluated their accuracy in terms of
%false positives and negatives. 
%However, this work argues that the evaluation can be misleading with their ad hoc
%threshold, since changing the {\it staleness} threshold results in significant differences of numbers of false positives and negatives.
%Later in Section XXX, we show the importance of accurate threshold determination
%in terms of resulting precision, recall, f-measure.
%The following shows how these metric reflects the false positives/negatives.


%This work argues that in general there is no such a problem in production
%environment such as datacenters. Note that many datacenter applications already
%collect various traces for a monitoring purpose, and dedicated analysis
%machines often exist in the datacenter to process the log and trace data of production machines. 
%Thus, Sniper can leverage such machines to enable the remote simulation.
%Figure~\ref{fig:center} shows the typical datacenter environment.
%In particular, both production and analysis machines share a distributed file system, e.g.,
%Google File System~\cite{gfs03}, which is connected to a separate a gigabit network. 
%Thus, generating a trace file
%rarely affects the QoS of the application which is serviced using another
%network, e.g., Internet; the production machines are equipped with two network interface cards for each separated network in order to
%keep the overhead minimal.
%Consequently, the trace file size is not an issue in the datacenter environment. 



