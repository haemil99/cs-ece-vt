\section{Related Works}

The problem of ensuring data consistency and forward execution progress for an
intermittent power system is at the heart of energy harvesting computing today.
Various methods have been proposed from hardware designs to software solutions.
As an executive summary,  no prior works provide the forward execution progress guarantee
as in RockClimb, and they provide the data consistency at the cost of increasing the
performance overhead, the power/energy consumption, and the hardware complexity.

\paragraph {Hardware Solutions} 
To solve the problem, researchers have designed the nonvolatile
processor~\cite{Wang_2012,LiuLLWLMLCJ0SY15,Ma_2015}.  Wang \emph {et al}  propose utilizing Nonvolatile Flip-Flops
(NVFF), as their fundamental block for register checkpointing ~\cite {Wang_2012}.
NVFF leverages a hybrid CMOS and ferroelectric technology in which 
a backup ferroelectric capacitors (FeCap) are coupled to a standard CMOS D latches.
Lui \emph {et al} expand on the work of Wang by additionally designing nonvolatile
SRAM; the design is roughly the same hybrid technique as NVFF ~\cite {Lui_2015}.
The benefit of such technologies would be a fast transfer of bit data because of
the close proximity to the storage source. But no such fabrication has been
adopted for production hardware yet. Thus, this leads to the problem that any
technology based on the nonvolatile processor approach will not be viable today.
In contrast, RockClimb does not rely on any future technology, so this problem is mitigated.  
Another issue with
this approach as shown by the research of Ma \emph{et al} is that NVFF
technologies require excessive energy to backup during N-Stage Pipeline backup
schemes \cite {Ma_2015}. 

\par One problem for ensuring consistency is determining what to backup.
The most basic approach is the backup everything at one time.  This approach is
given by Wang \emph {et al} for their design ~\cite{Wang_2012} and is labeled as
\emph {On Demand All Backup} \((ODAB)\) by Ma \emph{et al} ~\cite{Ma_2015}.
This leads to issues of excessive latency for backups and restores and high peak
energy consumption.  To mitigate this, several techniques are offered.  Lui
\emph {et al}  address this problem by compressing the area of NVFF with a
couple of different algorithms.  This leads to issues of excessive backup
latency as much as 50\% of total latency, with another algorithm given that
would reduce the latency by 76\% while increasing the area overhead by 17\%
~\cite {Lui_2015}.  In the research by Ma \emph {et al} it is suggested to
create a flag bit on each register to mark it as dirty and reset the bit after
backup, \emph{On Demand Selective Backup} \((ODSB)\)~\cite {Ma_2015}.  This
solution will ensure only the registers that need to be saved will.  But as
shown in their paper, ODSB loses efficiency to ODAB when power sources are
mildly unstable ~\cite {Ma_2015}.  This leads to the presumption that additional
hardware to sort or select dirty registers is not advantageous for mildly
intermittent energy harvesting systems. Since our compiler implements an
incremental backup scheme via static analysis, registers that are needed to be
backed up are done so with no additional hardware.  This leads to a minimized
solution to the problem and still ensures consistent execution.
\ignore{
\par
For all of the hardware solutions, the issue of when to trigger a backup has
limited answers.  In the research covered, the only solution given is that of
voltage monitoring ~\cite {Wang_2012, Lui_2015, Ma_2015}.  This leads to a
guaranteed backup if voltage drops below a threshold voltage. This method has
the vast benefit of restarting at the place where the fault occurred. But, this
also leads to yet more hardware in a limited energy system to power.
Additionally this produces a worse case scenario where the voltage level is
tightly bound to the threshold.  If the fluctuations never dip below zero volts
the system will continue to trigger backups unnecessarily, which will only
exacerbate the problem of limited energy.  In addition to this bulk capacitors
are proposed to either smooth out ripple or act a as storage buffer to ensure
backup ~\cite {Wang_2012, Lui_2015}.  This adds a latency to charge the
capacitor when resuming execution and general current leakage through the
dialectric.  Our compiler implements a checkpointing scheme and thusly does not
rely on voltage monitoring systems and will not incur such problems.  
}
\paragraph {Software Solutions}
			A couple of solutions are discussed that involve software solutions to the problem.  These solutions revolve around the correct placement of checkpoints and what occurs during checkpointing. 
			As for the correct placement of checkpoints, two starkly different approaches are given.  In the research by Xie \emph {et al}, the algorithms given are a heuristic to approximate minimal checkpoints for a given program.  This design revolves around the idea of severing anti-dependencies that appear in the code by placing a checkpoint between a load-store pair ~\cite {Xie_2015}.  This technique is in the same vein of what our compiler will implement.  As for Lucia and Ransford's compiler \emph{DINO}, this burden is placed on the programmer utilizing their compiler.  They are given a runtime library to place re-executable task boundaries and it is up to the programmer to make good judgment of whether a task is considered idempotent ~\cite {Lucia_2015}.  This design choice is simple in principle but adds even more complexity to the programming process.  Because our compiler does not expose such a mechanism to the language, no such burden is placed on the end user. 
			\par
			Determining what takes place during a checkpoint is a crucial problem for performance and energy efficiency.  Both Xie \emph {et al} and Lucia and Ransford solved this problem by “blindly” backing up the register file ~\cite{Xie_2015, Lucia_2015}.  This ultimately will lead to the same issue of high peak energy as the hardware approach ~\cite {Wang_2012}.  They differ in how to solve system memory consistency.  In the proposal by Xie \emph {et al}, the system RAM is given to be nonvolatile ~\cite {Xie_2015}.  \emph {DINO} implements a more complex approach by utilizing regular SRAM and only copying back the memory to NVRAM when a memory address is dirty at reaching a checkpoint.  They refer to this as data versioning, and only record the most relevant version to NVRAM ~\cite {Lucia_2015}.  This is too of the same vein as to our research.  Our compiler will focus on registers/memory addresses that are live-in to the next region after a checkpoint.  As stated before this principle of liveness and incremental checkpointing will greatly reduce the peak energy used and minimize nonvolatile writebacks.




