
%C.4 Proposed Research and Preliminary Results (6-7 pages)

We divide the proposed research into three main focus areas: task
identification and partitioning at the source code level
(DynaMight), from the binary level (DynaSoar) and task
partitioning/scheduling using the hardware feedback system (AnoDyne).

First, we summarize task partitioning and illustrate
task partitions in consideration of hardware resources. We will
describe the detailed proposed mechanisms in the later sections.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{C.4.1 AnoDyne: Task Partitioning and Management}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\subsubsection*{C.4.1 Task Partitioning Based on the Hardware Requirements}

{\bf Partitioning Based on Application Characteristics:} Many programs
have complex control-flow characteristics. Figure
illustrates different program behaviors in one application. The
program has sequential sections, parallel sections, and massively
parallel sections. The goal of the task management system is to
identify the characteristics of each section and then assign each task
to an appropriate processor. In this example, we consider 4 high
performance CPU processors and a massively parallel GPU
processor. There are four cases; Case (a): The code section is serial,
so the task is sent to the CPU core.  Case (b): The code section has
parallel sections but the parallel code has complex control
flow. Hence, the tasks are sent to only the high performance CPU
cores. Case (c): The code section has high data parallelism. The
scheduler sends all the tasks into the GPU. Case (d): The code section
has both complex control flow graph tasks and massively data
parallelism tasks. The scheduler sends tasks to both the CPU and the
GPU.


%\begin{figure}[htb]
%\begin{minipage}{3.5in}
%\centering
%\includegraphics[width=3in,angle=-90]{decomp.pdf}
%\caption{Task partitioning and scheduling}
%\label{fig:task}
%\end{minipage}
%\hfill
%\begin{minipage}{2.5in}
%\centering
%\includegraphics[width=2.4in]{cuda.pdf}
%\caption{Performance of using CPU vs. GPU}
%\label{fig:cuda}
%\end{minipage}
%\end{figure}



{\bf Partitioning Based on Task Size:} Even though a task has high data
parallelism, the task management system also needs to consider the
length of the task, in order to use the current generation of GPU
systems. In current CPU and GPU systems, data is transferred to/from
the GPU through a PCI-Express bus.  The communication latency is much
higher than that of the shared memory system.  Hence, the task
scheduler should consider the cost of data transfer before it decides
to use the GPU, and the task size should be long enough to amortize the
cost of sending the work to the GPU. Figure shows
the trade-off between using the CPU and the GPU. The application
computes nearest-neighbor averages on a rectangular grid after using applying
computation intensive filter. The result shows the speedup compared to
a sequential version of the code. The execution time of CPU is
measured on an Intel 1.87GHz 8 core system, and the execution time at
GPU is measured at NVIDIA G80 architecture (8800 GTX). 
When the grid dimension is less than 128, using multiple CPUs
provides higher performance than using the GPU, mainly because of the
cost of sending data to the GPU. However, the performance of using
multiple CPUs saturates at around 7.4 (the system has only 8 cores)
but the performance benefit of using the GPU increases as the array
size increases.  When the array size is 512, the GPU provides about
40$\times$ speedup while the 8 core processor only provides 7.4$\times$
speedup. Hence, the task management system should consider the size of
tasks and decide when to use CPU or GPU.




{\bf Partitioning Based on Critical Paths:} The execution time of each
task within an application may vary. Tasks on the critical path of the
task graph should be executed in fastest cores. The critical tasks may
be known statically or may be dependent on the run-time behavior. For
instance, programs which are written using OpenMP often have similar
execution times for all threads. However, many control-flow intensive
applications often have a complex mix of critical and non-critical
tasks.

%\begin{figure}[htb]
%\begin{minipage}{3in}
%\centering
%\includegraphics[width=2.5in,angle=-90]{program_affinity.pdf}
%\caption{Example of critical/non-critical tasks}
%\label{fig:aff}
%\end{minipage}
%\hfill
%\begin{minipage}{3in}
%\centering
%\includegraphics[width=2.7in]{scheduler.pdf}
%\caption{Performance comparisons of scheduler algorithms}
%\label{fig:asym}
%\end{minipage}
%\end{figure}


Figure compares the performance of matrix multiply
between a critical task-aware scheduler and a round-robin scheduler on
a heterogeneous architecture. We emulated the heterogeneous
architecture by changing the frequency of cores. The system has 8
cores: 4 cores that have a 1.87GHz (the faster core) and 4 cores have
a slower 1.6GHz.  The matrix multiply computation is organized into
threads with differing task lengths: 4 threads have longer tasks
(critical path tasks) and 4 threads have shorter tasks (non-critical
tasks). When the application creates a child thread, it also notifies
the scheduler whether the the thread is critical or not.  The critical
task-aware scheduler sends critical tasks to the faster cores and the
non-critical tasks to the slower cores, whereas the round-robin
scheduler only tries to balance the overall load. We see that the
critical task-aware scheduler can improve the performance by 14.8\%,
which compares well to the raw performance difference between faster
core and slower core of about 16.8\%. When the performance difference
between cores increases, we expect the performance benefit of the
critical path-aware task scheduler also to increase.


{\bf Partitioning Based on Resource Contention:} A task scheduler
should consider shared resource contentions. In particular, the task
partitioner and the scheduler should decide the optimal number of
threads and where the threads should be executed.  For example,
consider Figure, which illustrates two scenarios in
which threads are limited by having to execute critical sections. Each
thread needs to enter the critical section to update a shared data
structure, such as a hash table for each loop iteration. When the
critical section is short (case (a)), all three threads perform useful
work all the time. However, when the critical section is long (case
(b)), threads waste most of their execution time waiting to enter the
critical section. In this case, the task partitioner should reduce the
number of active threads to save resources and to save energy.


%\begin{figure}[htb]
%\centering
%\includegraphics[width=1.5in,angle=-90]{critical.pdf}
%\caption{Critical section aware task partitioner}
%\label{fig:critical}
%\end{figure}






%C.4 Proposed Research and Preliminary Results (6-7 pages)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{C.4.2 DynaMight: Dynamic Task-Graph Partitioning and Tuning}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We have discussed the basic overviews and examples of task
partitioning algorithms. In this section, we describe the component of
our proposed work that will investigate the foundations for
application- or algorithmic-level task partitioning, where we
partition to simultaneously optimize both available parallelism and
data locality.  More specifically, we will consider two research
problems:
%
\begin{enumerate}
  \item \textit{Static and dynamic partitioning for parallelism and
  locality}: We propose to investigate a new task- and data-graph
  model that allows us to partition for both
  parallelism and data locality.
%
  \item \textit{Task graph tuning}: Given information at the
  algorithmic-, library-, or application-level, combined with feedback
  from the run-time system and/or underlying hardware, we will
  investigate tuning tasks on-the-fly as each task is generated.
\end{enumerate}
%
To explain these ideas, we introduce an example taken from
dense matrix computations. Though this area is
considered well-understood, recent work suggests that new asychronous
formulations can yield 3--4$\times$ performance
moreover, we believe this prior work both applies more broadly and suggests new opportunities to design
new partitioning and tuning techniques.


\vspace{.1in}
\noindent
{\bf Algorithm example: Asynchronous Cholesky factorization.} Given a
symmetric matrix $A$, the goal of a Cholesky factorization is to
compute a lower-triangular matrix $L$ such that $A=LL^T$. Factoring
$A$ is the dominant step in solving a linear system of equations
$Ax=b$. This routine is typically parallelized and implemented in a
bulk-sychronous style, for instance as implemented
ScaLAPACK. More
recently, researchers have revisited asynchronous formulations for
multicore
platforms.
These asychronous formulations come naturally (and even
mechanically) from the mathematics: In
Figure, we show the progression from writing down
the mathematical goal (top-left); identifying the key algorithmic
steps, or ``tasks'' (bottom-left); and finally the iterative algorithm
(right). The key observation is that the algorithmic structure leads
immediately to three distinct tasks, with some inter-iteration
data-dependencies among steps.

To obtain a cache-friendly implementation, we simply let each
$A_{i,j}$ in the algorithm of Figure (right) be a
submatrix or block, rather than a single element. To obtain an
asychronous parallel implementation, we let each of the
steps---labeled ``factor,'' ``triangular solve,'' and ``update''---be
distinct spawnable tasks that produce and consume data, with the
shared data serving as natural sychronization points. The resulting
task graph appears in Figure (top), which
essentially depicts a data-flow execution
model.

%\begin{figure}
%\centering\begin{minipage}{3.5in}
%\begin{eqnarray*}
%A & = & LL^T \\
%\left(\begin{array}{cc} A_{11} & A_{21}^T \\ A_{21} & A_{22} \end{array}\right)
%& = & \left(\begin{array}{cc} L_{11} & \\ L_{21} & L_{22} \end{array}\right)\cdot\left(\begin{array}{cc} L_{11} & \\ L_{21} & L_{22} \end{array}\right)^T \\
%& \Downarrow & \\
%\mbox{1. {\color{red}Factor}: } && L_{11}L_{11}^T \leftarrow A_{11} \\
%\mbox{2. {\color{blue}Scale}: } && L_{21} \leftarrow A_{21}L^{-1}_{11} \\
%\mbox{3. {\color{green}Update and factor}: } && L_{22}L^T_{22} \leftarrow A_{22} - L_{21}L_{21}^T
%\end{eqnarray*}
%\end{minipage}
%\qquad
%\begin{minipage}{3in}
%  \begin{tabbing}
%\quad\=\quad\=\quad\= \\
%// {\color{red}\textit{Loop over columns}} \\
%\textbf{for} $j=1$ \textbf{to} $n$ \textbf{do} \\
%\> $A_{j,j} \leftarrow \mbox{{\color{red}factor}}(A_{j,j})$ \\
%\> // {\color{blue}\textit{Scale column $j$ below diagonal}} \\
%\> \textbf{forall} $i: j < i \leq n$ \textbf{do} \\
%\> \> $A_{i,j} \leftarrow \mbox{{\color{blue}triangular-solve}}(A_{i,j}, A_{j,j})$ \\
%\> // {\color{green}\textit{Update trailing sub-triangle}} \\
%\> \textbf{forall} $(i, k): j < k \leq i \leq n$ \textbf{do} \\
%\> \> $A_{i, k} \leftarrow \mbox{{\color{green}update}}(A_{i,j},A_{k,j})$
%\end{tabbing}
%\end{minipage}
%\caption{
%%
%(\textit{Left}) Computing the Cholesky factorization of $A = LL^T$ of
%  a symmetric matrix $A$ consists of three basic steps.
%%
%(\textit{Right}) These steps imply an iterative algorithm, which can
%  be performed in-place. We obtain efficient cache behavior by
%  consider each $A_{i,j}$ to be a block. From a parallelism /
%  data-flow perspective, we need only ensure that the block operands
%  required for each of the three steps (factor, scale, and update) are
%  ready.
%%
%}
%\label{fig:cholesky}
%\end{figure}

%\begin{figure}
%\centering\includegraphics[width=6in]{./cholesky-taskgraph-n4.pdf}
%\vspace{.1in}
%\centering\includegraphics[width=6in]{./cholesky-datagraph-n4.pdf}
%\caption{
%%
%(\textit{Top}) Cholesky task graph, for $n=4$. Triangles, circles, and
%boxes represent the factor, triangular-solve, and update steps of
%Figure~\ref{fig:cholesky}.
%%
%(\textit{Bottom}) Cholesky data locality graph, for $n=4$. Each box
%represents the $A_{i,j}$ entry (block-entry) of $A$ as indicated by
%the coordinates. A blue edge $(u, v)$ indicates that a
%triangular-solve task consumes block $u$ and produces block $v$;
%similarly, green edges show production/consumption for an update task.
%%
%}
%\label{fig:cholesky:graph}
%\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{.1in}
\noindent
%
{\bf Dynamic task-graph partitioning:}
%
We first consider the problem of how to schedule and load-balance a
task graph, which is naturally formulated as either a graph
partitioning problem (if the tasks and their dependence relations are
known in advance) or as a self-scheduling problem (for dynamically
evolving task graphs).  In both cases, there is an extensive
literature with many theoretical results.
The high-performance scientific computing, compilation, and operations
research/theoretical computer science communities originated much of
this research, though many of the ideas have crossed-over into recent
compiler- and microarchitecture-based work, particularly for
scheduling on clustered
microarchitectures~\cite{chu03:2,aleta02,chu06}.

The goal of our proposed work is to develop new techniques for
\textit{locality-sensitive partitioning and scheduling of dynamic task
graphs}. To provide some context for the proposed approach, we first
briefly survey existing techniques.

If the task graph is known in advance, graph partitioning techniques
may be applied directly, given a suitable definition of node and edge
weights (typically work and communication estimates, respectively).
Though the problem is computationally hard, many good heuristics
exist. Notably among these are heuristics for planar and other
geometrically ``well-shaped''

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{C.4.4 Closing the Feedback Loop: A Hardware Monitoring System}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To close the dynamic feedback loop between the application
and the task partitioning and management system, we see a
significant opportunity to design an innovative hardware monitoring
system.

Consider that the task management system needs a wealth of information
about the executing application, including the size of each task,
whether tasks have high data parallelism or not, which tasks are
critical tasks, the length of critical sections, and other resource
requirements. We propose to investigate an efficient run-time
monitoring system.


The basic framework of the run-time monitoring system is that the hardware
provides a signature for each task and the monitoring system stores
the collected data with the associated signature in either hardware or
in the memory system. The characteristics of tasks can be learned by
collecting the information during run-time.

Resource requirements would be collected using existing hardware
performance counters, such as cache misses, memory bandwidth
consumptions, processor's occupancies, DRAM page hit/miss
ratios. However, we will consider measuring application activity
beyond what hardware counters provide.  Critical path tasks will be
identified using the last arrival time. Whenever there is a barrier,
the task management system collects which thread is the last arrival
thread. It will also consider the case of multiple critical tasks. The
execution time of critical sections can be monitored from the task
itself. The task management system can also collect the information if
threads use system calls to enter the critical section. All the
collected information will be used in the task management system and
more effective way of communicating will be studied during this
project.






% LocalWords:  spawnable sychronization Lucco Karp randparbb Blumofe worksteal
% LocalWords:  Cybenko dynloadbal Oliker Schloegel loadbal diffpart geosepfem
% LocalWords:  Kernighan graphpart Fiduccia ZoltanParHypRepart Ghosh diffbal
% LocalWords:  microarchitecture microarchitectures partite hypergraphs AnoDyne
% LocalWords:  multigrid asychronously tunability DynaMight DynaSoar Cholesky
% LocalWords:  ITK interpolator parallelizable GetDerivatives al GPU PCI GHz Hu
% LocalWords:  GetValueAndDerivatives MutualInformationImageToImageMetric GTX
% LocalWords:  MattesMutualInformationImageToImageMetric NVIDIA OpenMP CPUs
% LocalWords:  manycore runtime bytecode parallelizing Ryoo et al analyses FPGA
% LocalWords:  interprocedural lookups Valgrind partitioner parallelization
% LocalWords:  tradeoff tradeoffs FPGAs sychronous asychronous submatrix forall
% LocalWords:  parallelized ScaLAPACK multicore qmodels diffusively autotuned
% LocalWords:  online retarget
