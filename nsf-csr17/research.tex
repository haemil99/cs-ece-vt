
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Memory Leak, the Background and the Motivation}
\label{overview}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Memory leaks are common causes of a security breach.
They are leaks are particularly notorious, since their symptoms and causes
are insidious and hard-to-track~\cite{netherco07,hastings92}.  Leaks occur when
allocated objects are not freed, even if they are never accessed again. Since
they remain allocated consuming the heap memory, they gradually affect the
quality-of-service (QoS) of the system. 
Even worse, piled leaks eventually
crash applications by exhausting system resources.  Memory leaks can also result
in software security/reliability problems (CWE-401)~\cite{cwe13}. For example,
many CVE entries including CVE-2013-0152/0217/1129 have detailed the
problems~\cite{cve13}, and malicious exploits have been designed based on memory
leaks to launch denial-of-service (DoS)
attacks~\cite{whittaker04}.  

% leak is getting common
In the manycore era, leaks are more common than ever in
multithreaded software.  When heap-allocated objects {\it escape} their thread,
it is hard to determine when and which thread is to deallocate them.  Due
to the difficulties of reasoning about the {\it liveness} of the shared objects,
programmers often end up leaving the objects allocated in the memory thereby
producing leaks.  Despite undergoing extensive in-house testing, leaks often exist in
deployed software and show up in customer usage~\cite{bruening11,bond09}.  In
fact, they are common causes of bug reports for production software~\cite{firefox12,squid}.


With the model of infrastructure-as-a-service,
memory leak detection is one of the most critical issues in bare-metal
clouds. 
Several reasons drive this movement.  First, the threat of service downtime due to leaks 
has been a constant concern in cloud datacenters~\cite{dc02,dc03}. Such a
service-level-agreement (SLA) violation leads to the penalty, e.g., a reduction in fees~\cite{sla02}.

Second, if a single tenant runs multiple applications on a dedicated machine in the cloud datacenter, one leaking
application can threaten the QoS and the reliability of every other application running on
the same machine.  I.e., leaks impact not only the leaking application but also all
the others, due to the limited amount of available system
memory~\cite{clause10}.

Third, memory leaks directly affect the operational cost of the cloud
datacenter;
the fact that the service applications can be leaky puts significant
pressure on resource over-provision in the datacenter.  Once memory is actually
leaking, the cloud datacenter ends up consuming more and more resources
 to deliver the necessary performance as promised in the SLA. 


Lastly, the cloud provider needs not only to detect the threat
of leaks but also to correctly attribute it to the leaking application; 
just consuming large memory should not be blamed unless the application is leaking.
That is necessary to adjust the SLA and better support it rather
than to blame for the memory leak.  E.g., after fixing the leak, the customer
can run the service with a lower cost while the provider can allocate less
resource to it.  Thus, effective memory leak detection can improve the cloud
ecosystem by helping the provider as well as the customers.


\input{back}

\paragraph{Previous Work, Not a Solution for Bare-Metal Clouds}
Unfortunately, existing
tools~\cite{hastings92,netherco07,omega08,clause10,chilimbi04,novark09,bruening11}
cannot be used in bare-metal clouds for many reasons.  First, the tools cannot meet
the QoS demand due to their high overhead.  While
%e.g., $<$ 5\% latency and throughput degradation~\cite{mars11micro}, due to their high overhead.  While
state-of-the-art tools leverage sampling techniques to track accesses to heap
objects~\cite{chilimbi04,novark09}, the resulting overhead is still
unacceptable, e.g., 9.72x slowdown and more than 70\% dynamic memory increase for
 heap-bound applications.

Note that such memory-consuming approaches including~\cite{novark09,qin05} are
prohibitive in the clouds.
In reality, even 5\% increase of heap size due to faster memory allocation makes it impossible to use the memory
allocator in enterprise systems.
%allocator in enterprise systems~\cite{rus10}.
%In that sense, changing the underlying memory allocator is prohibitive, since this directly affects (de)allocation speed as well as fragmentation. 
Apart from that, {\it it just makes no sense to spend more memory for less leak}.

%To the best of our knowledge, existing tools cannot effectively deal with multithreaded software. It is much harder
%to effectively detect leaks in multithreaded software, since the overheads are likely to be worse as the number of threads grows.
%This is problematic because many datacenter applications are heavily multithreaded to
%increase throughput. 

%Especially, existing tools require unacceptable change in the original system.
%E.g., changing the memory allocator~\cite{novark09} is prohibitive,
%which directly affects (de)allocation speed as well as fragmentation.
%Similarly, compromising the memory reliability~\cite{qin05} is not acceptable.


%Besides, the tools can produce inaccurate results not only
%because of the difficulty of tracking parallel heap accesses, but because of the
%broken assumption of sequential execution.  E.g., to reduce false negatives,
%some tool
%relies on age-based heap segregation which does not consider multithreaded
%execution~\cite{novark09}. 

%Such inability to cope with
%multithreaded software is another major reason of preventing the use of existing
%tools in production environment. 

%Previous approaches cannot be used in production env(3); not systematic/automated
More importantly, existing tools are neither systematic nor automated. 
Their leak determination relies on a manually-set threshold. 
%Even if they require user to set it properly, there has been no methodology to do that.
%This is another reason of preventing their use in datacenters because 
That is, user intervention is required for each service, and even worse such a high cost will have to be paid
anew on environmental change, e.g., SLA adjustment or microarchitecture change.
It is unrealistic for cloud service providers to ask the customer to provide the
threshold for every service/SLA/architecture
combination.

The lack of a methodology to determine the threshold forces users to
do that properly, or ends up blindly applying a fixed threshold to those applications that have different characteristics.
As a result, existing tools can falsely blame non-leaking objects or miss real leaks.
I.e., the tools inherently vulnerable to false positives and negatives.

%Often time, they end up blindly applying a fixed threshold to those applications that have different characteristics.


%Often time, they end up blindly applying a fixed threshold to those applications that have different characteristics.
%In addition, it should be noted that such an improper threshold
%directly impacts the quality of memory leak detection, i.e., generating false positives and negatives.  

%\TODO{point out the accuracy problem due to sampling; in particular if we
%sumbit the paper to ISCA, we should how pmu ends up with low accuracy.
%Existing {\it staleness-}based tools including Sniper leverage sampling
%  techniques. This means that the tools could miss
%    some memory accesses.  Due to the uncaught accesses to heap objects,
%      the tools might falsely report the objects as memory leaks, thus causing
%      false positives.
%}

{\it Given all this, there is a compelling need for a practical memory leak detection
tool usable in bare-metal clouds.} With that in mind, the proposed research seeks to
address the following three tasks:


\paragraph{Task1: Trace-Based Lightweight Memory Monitoring}
For a lightweight memory leak detection tool usable in bare-metal clouds,
it is required to have a low-overhead mechanism to monitor memory accesses.
We will attack the key sources of the runtime (space) overhead in the state-of-the-art {\it
staleness} based leak detector, which are summarized as:
(1) the instruction instrumentation to track accesses to heap objects
causes high runtime overhead. (2) most of the space overhead comes from tag (meta) data that abstracts the heap
objects; for each heap
object, tools need to maintain the {\it staleness}, the
allocation site, the dynamic program point that accessed the object, and the heap
organization information, e.g., the address range of the object.
(3) updating the {\it
staleness} of the heap objects causes both space and time
overheads. In particular,
for every sampled load, the tools need to determine if the
instruction accesses a heap object. 
This requires searching the tag directory
(which is a memory-intensive data
structure) for the
heap object whose range embraces the target address of the load instruction.
{\it A challenge here is to address each source of the overheads effectively.
Again, prohibited are the increases of both execution time and memory size in
bare-metal clouds. }

%Sniper addresses each source of the overheads effectively. 
%To remove the
%heavyweight instrumentation completely, Sniper exploits instruction sampling
%using hardware performance monitoring units (PMU) available in commodity processors.
%To reduce the space overhead due to the tag data, Sniper buffers the full trace of
%malloc/free and flushes each buffer into files when it
%is full. Similarly, Sniper maintains another buffer to keep information about PMU
%samples.
%Thus, the additional memory consumption is bound to the size of the buffers. 
%
%Sniper also offloads time- and space-consuming work of the {\it staleness}
%update to its trace simulator.
%Using the malloc/free/PMU traces generated at runtime as an input, the simulator
%performs the expensive {\it staleness} update offline.
%That way the memory-hungry tag directory and the space needed for the {\it staleness} are no longer necessary at runtime.
%Instead, it is during the offline simulation that a tag directory is constructed and searched for the {\it staleness} update.  
%In this manner, Sniper minimizes both time and space overheads during program
%execution by offloading much of the work.


\paragraph{Task2: Automated and Systematic Memory Leak Determination}
Another goal of Sniper is to provide a systematic and automated methodology for precise leak determination.
The lack of the methodology ends up blindly applying a fixed threshold to 
all the applications which may differ significantly in their behaviors.
We will leverage a couple of observations; (1) 
one-size-fit-all threshold does not exist even within the same application, i.e., 
 multiple thresholds should be carefully determined according to
 application characteristics. (2) separating objects based on their allocation site 
 where they are created, and then performing leak detection on the separated sets
can improve the accuracy, i.e., the inequality~\ref{eq:acc_cond} is more likely to hold.
{\it To this end, our leak determination methodology should be tailored for each
allocation-site as well as each application. This is a challenge to be
overcome.}


\paragraph{Task3: Data Structure Aware Object Separation }

The accuracy of the above leak determination methodology is affected by its
object separation scheme. For example, rather than just using the allocation site
information, we might be able to some other high level information about target program to improve
the accuracy of the methodology.
To this end, we plan to integrate and expand our previous work~\cite{jung09} on automatic
identification of data structures
to achieve better object separation using the data structure
knowledge.
The basic assumption is that data structure instance represents the memory
leakage behavior of those objects that belong to the instance.
For example, if two objects belong to different data structures, the memory
leakage behavior of the objects are likely to be different.




\input{details}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Task 3: Data Structure Aware Object Separation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Leveraging Data Structure Knowledge} 
In our prior work, we have developed a probabilistic {\bf D}ata
structure {\bf D}etection {\bf T}ool, or DDT, that can automatically
detect what type of data structures are used in program.  DDT
works by instrumenting memory allocations, stores, and function calls
in the target program.  Data structures are predominantly stored in
memory, and so instrumentation tracks how memory layout of a program
evolves.  Memory layout is modeled as a graph: allocations create
nodes in the graph, and stores create edges between graph nodes.  DDT
makes the assumption that access to memory comprising a data structure
is encapsulated by {\it interface functions}, that is, a small set of
functions that can insert or access data in the graph, or otherwise
modify nodes and edges in the graph.


Once the interface functions are identified, DDT uses the Daikon
invariance detection tool~\cite{ernst07} to determine the properties
of the functions with respect to the graph.  For example, an insertion
into a linked list will always increase the number of nodes in the
memory graph by one and the new node will always contain the value
being inserted.  We claim that together, the memory graph, the set of
interface functions, and the function invariants uniquely define a
data structure.  Once identified in the target application, the graph,
interface, and invariants are compared against a predefined library of
known data structures for a match, and the result is provided to the
user. We tested DDT on several real-world data structure libraries including the GNOME data
structure library and the Apache C++ Standard Library.
%structure library~\cite{gnome09} and the Apache C++ Standard Library~\cite{apache09}.
%, and a set of data structures used in the Trimaran research compiler~\cite{trimaran00}. 
Our previous work
demonstrates that DDT can accurately identify many types of data
structures~\cite{jung09}.


\paragraph{Future Work} 
By leveraging in-depth data structure knowledge, we can make a better
decision on object separation. 
For example, we can separate heap objects
corresponding to the backbone of linked data structures from their pointer data
value which is another heap object.
Thus, we plan to integrate the data structure
detection capability in this proposed work.
While this aspect of the proposed work is clearly more engineering than
research, there are still major research challenges to overcome.

DDT first needs to be improved to reduce its runtime overhead.  Even if DDT can
accurately identify many types of data structures despite implementation
differences~\cite{jung09}, it causes too much runtime overhead, most of which is
caused by the instrumentation part.  With that in mind, we plan to leverage
the same PMU hardware support called PEBS~\cite{nhmPMUGuide} available on commodity
processors to sample memory accesses to a data structure without actually instrumenting
them.

Even if it dramatically reduces the current overhead, it also presents
another problem. Due to the sampling of data structure accesses, DDT might
suffer from false positives/negatives. E.g., unsampled accesses of
critical store instructions which make a connection between data structure nodes
might end up with incorrect data structure detection results.

We believe that the problem can be solved by making up the information loss due
to the
sampling with additional heap trace generation.
As an example, rather than just tracing the address being freed, we can record
 the pointer content in the address by dereferencing it.
I.e., before freeing some object, we can still observe how it is connected to other
objects. 
Empirically speaking, leveraging such an additional free trace is useful for
speculating the shape of data structures, but a significant amount of
additional research is needed to further understand the utility and
limitations of this approach.




%\input{relwork}
